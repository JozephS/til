IMPORTANT
----------

1. Check git config SYSTEM

From (stackoverflow)[https://stackoverflow.com/questions/36339248/on-windows-git-pull-and-clone-for-google-cloud-repository-pops-credential-manage]
If you run

```
C:\> git config --list --system credential.helper=manager
```
and get a setting for credential.helper like above, it will have precedence over gcloud installed credential helper.

You can unset it via (possibly as administrator)

```
C:\> git config --system --unset credential.helper
```

Now

```
C:\> gcloud source repos clone default my_repo
```

should work and

```
C:\> cd my_repo
```
C:\my_repo> git config --list
should display

```
credential.helper="gcloud.cmd"
```


2. Same for other global & local
