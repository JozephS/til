## Notes

- Use break of trend structure via HH/LL breakout: https://www.youtube.com/watch?v=smMq_aRNj2s
- Don't fight the trend! Even though you already have a divergence, wait for trend structure break
 - Use High/Lows or use Close?
 - BOT: Use 30ema instead of trendline, no interpretation needed