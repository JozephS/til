---
kindle-sync:
  bookId: '14010'
  title: Reminiscences of a Stock Operator
  author: Edwin Lefevre
  asin: B08JZG1KTC
  lastAnnotatedDate: '2021-09-07'
  bookImageUrl: 'https://m.media-amazon.com/images/I/716hsusd-lL._SY160.jpg'
  highlightsCount: 10
---
# Reminiscences of a Stock Operator
## Metadata
* Author: [Edwin Lefevre](https://www.amazon.com/Edwin-Lefevre/e/B000APATZG/ref=dp_byline_cont_ebooks_1)
* ASIN: B08JZG1KTC
* ISBN: 1687866880
* Reference: https://www.amazon.com/dp/B08JZG1KTC
* [Kindle link](kindle://book?action=open&asin=B08JZG1KTC)

## Highlights
rule: In a narrow market, when prices are not getting anywhere to speak of but move within a narrow range, there is no sense in trying to anticipate what the next big movement is going to be up or down. — location: [1740](kindle://book?action=open&asin=B08JZG1KTC&location=1740) ^ref-4744

---
interest until the price breaks through the limit in either direction. — location: [1742](kindle://book?action=open&asin=B08JZG1KTC&location=1742) ^ref-14440

---
Let him buy one-fifth of his full line. If that does not show him a profit he must not increase his holdings because he has obviously begun wrong; he is wrong temporarily and there is no profit in being wrong at any time. The same tape that said UP did not necessarily lie merely because it is now saying NOT YET. — location: [1770](kindle://book?action=open&asin=B08JZG1KTC&location=1770) ^ref-36999

---
He must fear that his loss may develop into a much bigger loss, and hope that his profit may become a big profit. It is absolutely wrong to gamble in stocks the way the average man does. — location: [1825](kindle://book?action=open&asin=B08JZG1KTC&location=1825) ^ref-797

---
this: A man may beat a stock or a group at a certain time, but no man living can beat the stock market! — location: [1828](kindle://book?action=open&asin=B08JZG1KTC&location=1828) ^ref-27398

---
asinine. — location: [2176](kindle://book?action=open&asin=B08JZG1KTC&location=2176) ^ref-48408

---
Wall Street. When you read contemporary accounts of booms or panics the one thing that strikes you most forcibly is how little either stock speculation or stock speculators to-day differ from yesterday. The game does not change and neither does human nature. — location: [2551](kindle://book?action=open&asin=B08JZG1KTC&location=2551) ^ref-21084

---
never buy at the bottom and I always sell too soon.” — location: [2955](kindle://book?action=open&asin=B08JZG1KTC&location=2955) ^ref-9610

---
top make up for it by not taking profits. — location: [3815](kindle://book?action=open&asin=B08JZG1KTC&location=3815) ^ref-36583

---
The speculator’s deadly enemies are: Ignorance, greed, fear and hope. — location: [4123](kindle://book?action=open&asin=B08JZG1KTC&location=4123) ^ref-61878

---
