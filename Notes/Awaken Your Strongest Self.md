---
kindle-sync:
  bookId: '36419'
  title: Awaken Your Strongest Self
  author: Neil A. Fiore
  asin: B00422LBYG
  lastAnnotatedDate: '2017-03-22'
  bookImageUrl: 'https://m.media-amazon.com/images/I/71Rwa8cPibL._SY160.jpg'
  highlightsCount: 11
---
# Awaken Your Strongest Self
## Metadata
* Author: [Neil A. Fiore](https://www.amazon.com/Neil-A-Fiore/e/B000APJK5Q/ref=dp_byline_cont_ebooks_1)
* ASIN: B00422LBYG
* ISBN: 0071470263
* Reference: https://www.amazon.com/dp/B00422LBYG
* [Kindle link](kindle://book?action=open&asin=B00422LBYG)

## Highlights
“Yes, I’m here for you. You don’t have to know how to do this alone. Just show up focused in the present, and watch for the surprise.” — location: [1578](kindle://book?action=open&asin=B00422LBYG&location=1578) ^ref-4506

---
• I limit my stress reaction to less than thirty seconds. Self-threats are no longer acceptable. I communicate to every part of me, “Your worth is safe with me. Regardless of what happens, I will not make you feel bad.” • I manage my life from choice rather than the ambivalence that’s caused by the inner conflict between “you have to” and “I don’t want to.” Ambivalence and inner conflict are now wake-up calls for my Strongest Self to make an executive choice. • I rapidly move from an isolated, worrying conscious mind and shift it to wondering what the deeper wisdom of my larger, integrated mind and body will achieve. • I take at least three deep breaths before starting to work in order to connect with a deeper system of support, to link my left brain with my right brain, and to connect with the wisdom of my body. • I integrate every part of me into a powerful, focused team. No single part of me works alone or carries full responsibility for my life. • I, as my Strongest Self—not the six-year-old or the two-year-old or any of my lower brain functions—am in charge of my life. From the perspective, wisdom, and support available to me as my larger self, I take responsibility for my life and the role of guiding all parts of me toward my higher values and mission. — location: [1632](kindle://book?action=open&asin=B00422LBYG&location=1632) ^ref-29581

---
The next time you find yourself tightening your fists or your jaw, furrowing your brow, and starting to curse because something isn’t going your way, see how quickly you can exhale and float down into your chair, the soles of your feet, and the earth or the floor. (Note: a good time to practice this is when you’re stuck in traffic.) 2. When you exhale, open your hands as if to say, “It’s out of my hands. This is going to be interesting because my ego is clearly not in control of this situation.” (Note: practice this with your children, your parents, your boss, or your employees.) 3. Give yourself time to hold your breath and exhale at least three times. With each exhalation, let go of more tension and focus your attention on choosing to face the task before you as a fact of life, rather than cursing it or making it a problem. (Note: practice this when you’re procrastinating on a project that some part of you doesn’t want to do; remember, the self operates from choice—not “have to” or “want to.”) 4. Consider feeling grateful for this challenge that takes you beyond your ego’s perspective and puts you in touch with resources and support you didn’t know you had. (Note: practice gratitude when paying the bills, facing a writing block, or washing the dishes; especially, be grateful for how your larger mind and self can calmly and creatively work with all aspects of life—even those aspects that you initially resist.) — location: [1650](kindle://book?action=open&asin=B00422LBYG&location=1650) ^ref-61564

---
“I’ve finally shown up as the adult coach in my life. You no longer have to struggle alone. You can stop trying so hard to fix or change the past and your parents. You don’t have to perform perfectly in order to be accepted. I’m on your side, regardless of success or failure. I will never abandon you. Regardless of your difficulties and flaws, I accept and love you completely.” — location: [1697](kindle://book?action=open&asin=B00422LBYG&location=1697) ^ref-36918

---
• Yes, you believe you must struggle alone to survive and achieve. Yes, of course you don’t trust that there’s anyone else to help or support you. Yes, you have plenty of reasons to defend yourself and to fear enemies, problems, and hurts. Yes, you’ve been hurt by loss, criticism, and attacks. • And now I’m here; you’re not alone. Come here to connect with a deeper wisdom, safety, and peace. I will never abandon you. You’ll always have a home here. I’m strong enough to be with your feelings. Regardless of what happens, I won’t make you feel bad. • I’m choosing what to do. I know better than any friend, lover, or therapist what you’ve been through. Even though you’re worried, insecure, and imperfect, I accept and love you completely. — location: [1786](kindle://book?action=open&asin=B00422LBYG&location=1786) ^ref-38358

---
Inhale and, while keeping your chin level, roll your eyes upward as far as you can, with your eyelids opened or closed. Hold your breath while continuing to look up. Exhale slowly and completely, letting your eyes roll forward and down as you concentrate on the feeling of floating down into the chair and into the floor. You may notice a profound relaxation flowing down your body as you let your eyes relax and float down and as you look inward to make a commitment to your body and your life. In three breaths, each in three parts, say one of the following statements to yourself: • First breath. “This is my life and my body—working for me like a faithful servant twenty-four hours a day, every day.” (You can lift your thumb to indicate this first statement.) • Second breath. “Certain habits, beliefs, substances, and relationships are toxic to my body and the full experience of my life and its potential.” (You can bring your first finger and thumb together to indicate the joining of your first and second statements.) • Third breath. “I am committed to protecting my life and my body from all toxic habits, beliefs, substances, and relationships in order to experience my full vitality and potential.” (Join your middle finger to your thumb and first finger and press them together to signal a firm commitment.) — location: [1820](kindle://book?action=open&asin=B00422LBYG&location=1820) ^ref-35159

---
It’s the year 20___ and I’m ___ years old. Very little has changed in my life during the last five years. I’m still working on the same goals, facing the same problems, living in the same place, working at the same job, and struggling with the same relationship issues.” Now answer these questions: 1. How do you feel about your situation? 2. What absolutely must change in less than five years? Write down what must change and how soon (for example, in three months, six months, or one year). 3. Look back over the last five years to see if you’re still dealing with the same problems and goals as you were then. If you haven’t made changes in the last five years, consider the negative results of continuing this pattern for another five years. Will you gain another ten pounds, increase your risk of heart disease or cancer, or lose your ability to be active? Will you still be working at the same job on the same projects and feel frustrated and angry with yourself? 4. Identify what’s keeping you from considering a plan to make changes in your life. Remember, you’re not committing to change nor are you taking action. You’re merely considering what you need to know and feel in order to state your intention to change. You’re still at Stage I, making up your mind about the risks and benefits of change and wondering when you’ll be ready for Stage II, committing to change. — location: [2110](kindle://book?action=open&asin=B00422LBYG&location=2110) ^ref-64677

---
“We have a higher nature which includes the need for meaningful work, for responsibility, for creativeness, for being fair and just, for doing what is worthwhile, and for preferring to do it well.” — location: [2144](kindle://book?action=open&asin=B00422LBYG&location=2144) ^ref-30299

---
Confidence and motivation are irrelevant to your executive decision to show up and do the job. You don’t have to wait for your ego to feel confident, motivated, and all-knowing before your Strongest Self chooses to take action on achieving your goals. By performing the exercises in this book and applying the five qualities of your Strongest Self to your life, you’ve gained skills that improve your chances of moving beyond your ego’s lack of confidence and of achieving your goals (for example, losing weight, stopping smoking, or writing a book). — location: [2197](kindle://book?action=open&asin=B00422LBYG&location=2197) ^ref-62489

---
Remember, you have five qualities to replace major blocks: Safety replaces stress, worry, and anxiety. Choice replaces ambivalence, indecisiveness, procrastination, and inner conflict. Presence replaces the feeling of being overwhelmed. Focus replaces self-criticism and self-blame. Connection to the wisdom, compassion, and leadership of your Strongest Self replaces needless struggle and separateness. In addition to the five qualities, you have powerful strategies, such as these: Getting a fear inoculation shot to motivate you to face fear Holding committee meetings to eliminate self-sabotage and gain cooperation Shifting to the perspective, roles, and voice of your Strongest Self and out of a limited sense of who you are and what you can do — location: [2228](kindle://book?action=open&asin=B00422LBYG&location=2228) ^ref-27778

---
Mental rehearsals are best done with your eyes closed so you can see what occurs internally in your body, emotions, and imagery as life’s challenges come across your path. When you use imagery to practice confronting a stressful situation or an upsetting emotion for thirty seconds without reverting to your usual escape mechanisms, you earn a fear inoculation shot that lessens your fear of future challenges. — location: [2293](kindle://book?action=open&asin=B00422LBYG&location=2293) ^ref-10078

---
