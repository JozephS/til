# Greenscreen / Bluescreen no problemo


## TIL

1000 kW may be to low when trying to balance out bg green and actors with some kind of regconizeable light setup (e.g. hollywood lights instead of just everything bright like for TV)
because then you get noise from having to use higher iso (e.g. 800iso, f8.0, 1/50Hz)
always bring more lights

# Concise Best Practice Video

[Chroma best practice Video](https://www.youtube.com/watch?time_continue=573&v=Z07lKQg00Bg)

Key points to take away from above:
----------
* Use green or blue screen accordingly to shot (bg, actors, etc). green is more pixel density, blue is darker, doesnt reflect as much
* don’t overexpose greenscreen (get near colorbar green at ~50% luminosity)
* make sure screen is clean & without wrinkles
* avoid compress, use raw for chroma
* in CC suite (eg. davinci) bring screenfootage to same level (by removing other channels)

## Clarifing post-process from YT-comment

>   **Q:**You mentioned that you bring down the RED + BLUE channel causing the green channel to rise. Are you keying with this bizarrely graded footage?
>
>   **A:**
>   Rather bizarrely graded. I don’t knock down the blue and red
>   from the subject. I isolate the green of the screen first. I only minimize the
>   two unwanted colors from the screen and grade the subject normally. Then I
>   shift the green to match video green of color bars. This results in a very easy
>   key that can be applied to all my key shots. I developed the process as a time
>   saver really, but noticed you tend to get better keys as well. I’m editing a
>   project right now - all key shots. The technique is working so well that I’m
>   keying in my off line, no extra work. We have the background as a bed layer and
>   we just drop the shot on top of it and it keys like an online with no tweaking

#Setup for Shoot

![Light and BG Setup](https://bytebucket.org/JozephS/til/raw/ec2d39144d094863bfcb68a9d18b554ca62c03f0/Greenscreen/setup.JPG?token=a9cb2002db1c71b80dd5660215bcf9ca470913ad)

Short desc of key point in photo above:
-----------------------------------

Time and time again I observed, that you can make your life remarkably easier if you have enough space between background and subject, mostly to minimize blue/green bounce light spilled on subject. Also use big black bouncer to shield from light spill (this is mostly for the camera and is generally good practice )



# Keying in Resolve


*  Use Denoiser to smooth things
*  Invert Mask
*  Use `3D Qualifier` to catch all greens 
  * Invert Selection
  * Set `Ratio` to `>1` if there are thick borders
* Add `Alpha Output` and use input from Qualifier Nodes
* Power Window to Garbage Mask Lamps, Stands, etc.
* Remove some Spill on Face, etc, bei qualifying and then offsetting -Reds

There is a `Resolve Greenscreen Key Node.zip` file with Nodes as a reference


![Resolve Nodes](https://bitbucket.org/JozephS/til/raw/529818b58b5ec0ed1aaa778f6f14a78751784abe/Film/Greenscreen/NodesSetup.png)

# Keying in After FX

![After FX Settings and Plugins](https://bytebucket.org/JozephS/til/raw/ec2d39144d094863bfcb68a9d18b554ca62c03f0/Greenscreen/AfterFX.png?token=9b59e9a9b5604d2577d1669f1f749c8342c04aed)

Battle-tested Plugins:
--------------

* Keylight
* Denoiser II (Red Giant Magic)

Step-by-Step:
-------

1. Remove roughly areas you don’t need with select and make new comp
2. Use Keylight on this comp
3. Use Denoiser if needed

