---
kindle-sync:
  bookId: '26871'
  title: 'Dune: The Gateway Collection (Gateway Essentials Book 302)'
  author: Frank Herbert
  asin: B00A7C9EVM
  lastAnnotatedDate: '2022-03-10'
  bookImageUrl: 'https://m.media-amazon.com/images/I/81L30ODWjML._SY160.jpg'
  highlightsCount: 4
---
# Dune
## Metadata
* Author: [Frank Herbert](https://www.amazon.com/Frank-Herbert/e/B000APO5OM/ref=dp_byline_cont_ebooks_1)
* ASIN: B00A7C9EVM
* Reference: https://www.amazon.com/dp/B00A7C9EVM
* [Kindle link](kindle://book?action=open&asin=B00A7C9EVM)

## Highlights
Paul glanced at Halleck, but the minstrel-warrior — location: [2543](kindle://book?action=open&asin=B00A7C9EVM&location=2543) ^ref-28500

---
psychokinesthetic extension, becoming a mote-self — location: [8060](kindle://book?action=open&asin=B00A7C9EVM&location=8060) ^ref-44797

---
emotions, to transform envy into enmity,” — location: [12221](kindle://book?action=open&asin=B00A7C9EVM&location=12221) ^ref-54599

---
Baptist John, — location: [20646](kindle://book?action=open&asin=B00A7C9EVM&location=20646) ^ref-17910

---
