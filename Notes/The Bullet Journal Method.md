---
kindle-sync:
  bookId: '38198'
  title: >-
    The Bullet Journal Method: Track the Past, Order the Present, Design the
    Future
  author: Ryder Carroll
  asin: B07B7C4F9C
  lastAnnotatedDate: '2020-12-07'
  bookImageUrl: 'https://m.media-amazon.com/images/I/91cCB6LfPLL._SY160.jpg'
  highlightsCount: 4
---
# The Bullet Journal Method
## Metadata
* Author: [Ryder Carroll](https://www.amazon.com/Ryder-Carroll/e/B07D82J4WC/ref=dp_byline_cont_ebooks_1)
* ASIN: B07B7C4F9C
* ISBN: 0525533338
* Reference: https://www.amazon.com/dp/B07B7C4F9C
* [Kindle link](kindle://book?action=open&asin=B07B7C4F9C)

## Highlights
Why is this important? Why am I doing this? Why is this a priority? — location: [1555](kindle://book?action=open&asin=B07B7C4F9C&location=1555) ^ref-39526

---
What small step can I take now to move this forward? What could I improve now? — location: [1962](kindle://book?action=open&asin=B07B7C4F9C&location=1962) ^ref-22376

---
What exactly did not work? Why did it not work? What small thing can I improve next time? — location: [1972](kindle://book?action=open&asin=B07B7C4F9C&location=1972) ^ref-1116

---
“Plan → Do → Check → Act.” Let’s break that down. Plan: Recognize an opportunity and plan a change. Do: Put the plan into play and test the change. Check: Analyze the results of your test and identify what you’ve learned. Act: Act on what you’ve learned. If the change didn’t work, go through — location: [1989](kindle://book?action=open&asin=B07B7C4F9C&location=1989) ^ref-15114

---
