#jedi-vim

`davidhalter/jedi-vim` 
for Python code completion based on Jedi, a Python language server.
###CoC — Conquer of Completion
I use coc.nvim with coc-jedi, which is a coc.nvim wrapper for jedi-language-server. 😊

https://samroeca.com/coc-plugin.html#coc-plugin

#fullscreen terminal
Switching between Vim and a full-screen terminal is so convenient that it’s worth creating a mapping in .vimrc:

`nnoremap <leader>t :stop<CR>`


#PYTHON

https://towardsdatascience.com/getting-started-with-vim-and-tmux-for-python-707ec5ff747f

https://vi.stackexchange.com/a/19234/33347
https://github.com/kassio/neoterm


https://github.com/bfredl/nvim-ipy

https://pythonrepo.com/repo/hanschen-vim-ipython-cell-python-command-line-tools


#DIFF

I used to feel that gvim was a big improvement for viewing diffs, but I've changed the background colour of my terminal to a dark non-black shade, and set

```
:highlight DiffAdd ctermbg=Black
:highlight DiffChange ctermbg=Black
:highlight DiffDelete ctermbg=Black
:highlight DiffText cterm=Bold ctermbg=None
```
The result of this is that in diff mode, differing text shows up with a black background, and unchanged text is coloured with the terminal background colour. For side-by-side diffs, this works wonderfully, since you can tell immediately based on the other side whether a given line is a change or add; for non-side-by-side you will be able to see an unchanged part in a changed line.

This means that you can leave syntax colouring on and still be able to see diffs. Again, you do need to be able to set the background colour of the terminal to a unique, dark, non-black shade. This facility is available in the terminal emulators that 


#NETRW

Netrw has some annoying defaults though. It’s recommended to put these two additional netrw settings in .vimrc. The first setting allows to open a file in a right split. The second setting suppresses netrw from saving .netrwhist files in the .vim folder.

`let g:netrw_altv = 1`
`let g:netrw_dirhistmax = 0`

https://github.com/theHamsta/nvim-dap-virtual-text


