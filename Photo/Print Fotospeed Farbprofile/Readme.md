Print at Fotospeed.at
========================


##Lessons learned:
- "Belichtung" ist nicht farbecht zum Monitor, -> Colors are more greenish on print than on AdobeRGB and sRGB (see pic)
- Use "Fotodruck"
- Use Profile AdobeRGB (more colors, 24 or 48bit)
- Use Monitor Profile sRGB (darker black, less saturated colors, esp reds)
