---
kindle-sync:
  bookId: '26705'
  title: The Martian
  author: George Du Maurier
  asin: B004TQEN4U
  lastAnnotatedDate: '2015-10-05'
  bookImageUrl: 'https://m.media-amazon.com/images/I/91E4-w+PelL._SY160.jpg'
  highlightsCount: 5
---
# The Martian
## Metadata
* Author: [George Du Maurier](https://www.amazon.com/George-Du-Maurier/e/B001IXO3C4/ref=dp_byline_cont_ebooks_1)
* ASIN: B004TQEN4U
* Reference: https://www.amazon.com/dp/B004TQEN4U
* [Kindle link](kindle://book?action=open&asin=B004TQEN4U)

## Highlights
your motives. If you love me as you — location: [3651](kindle://book?action=open&asin=B004TQEN4U&location=3651) ^ref-22997

---
and excavators, great irrigators, great — location: [4294](kindle://book?action=open&asin=B004TQEN4U&location=4294) ^ref-59845

---
our reward — location: [4390](kindle://book?action=open&asin=B004TQEN4U&location=4390) ^ref-58622

---
blancheur de lis, with just a point of carmine — location: [4588](kindle://book?action=open&asin=B004TQEN4U&location=4588) ^ref-38594

---
commissariat — location: [5276](kindle://book?action=open&asin=B004TQEN4U&location=5276) ^ref-56593

---
