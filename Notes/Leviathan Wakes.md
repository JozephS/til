---
kindle-sync:
  bookId: '1394'
  title: 'Leviathan Wakes: Book 1 of the Expanse (now a Prime Original series)'
  author: James S. A. Corey
  asin: B004XCGKYQ
  lastAnnotatedDate: '2021-02-15'
  bookImageUrl: 'https://m.media-amazon.com/images/I/910TAjw6e7L._SY160.jpg'
  highlightsCount: 1
---
# Leviathan Wakes
## Metadata
* Author: [James S. A. Corey](https://www.amazon.com/James-S-A-Corey/e/B004AQ1W8Y/ref=dp_byline_cont_ebooks_1)
* ASIN: B004XCGKYQ
* ISBN: 1841499889
* Reference: https://www.amazon.com/dp/B004XCGKYQ
* [Kindle link](kindle://book?action=open&asin=B004XCGKYQ)

## Highlights
what he was seeing make sense. He’d seen men eviscerated — location: [5415](kindle://book?action=open&asin=B004XCGKYQ&location=5415) ^ref-27405

---
