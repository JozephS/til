---
kindle-sync:
  bookId: '49428'
  title: Battle Ground (Dresden Files Book 17)
  author: Jim Butcher
  asin: B0867ZMV2S
  lastAnnotatedDate: '2022-02-08'
  bookImageUrl: 'https://m.media-amazon.com/images/I/91LqFQrrc5L._SY160.jpg'
  highlightsCount: 2
---
# Battle Ground
## Metadata
* Author: [Jim Butcher](https://www.amazon.com/Jim-Butcher/e/B001H6U718/ref=dp_byline_cont_ebooks_1)
* ASIN: B0867ZMV2S
* ISBN: 0593199308
* Reference: https://www.amazon.com/dp/B0867ZMV2S
* [Kindle link](kindle://book?action=open&asin=B0867ZMV2S)

## Highlights
And humans can only bear tension, fear, and worry for so long. We aren’t built to sit quietly under such burdens. We’re built to go out and deal with whatever is causing them. We aren’t built to sit and take it. We were made to take action. Eventually, too much pressure will bring a willing fight out of anybody. — location: [4781](kindle://book?action=open&asin=B0867ZMV2S&location=4781) ^ref-61719

---
When people you know die, that gets attention. That was the beginning of the change — location: [5650](kindle://book?action=open&asin=B0867ZMV2S&location=5650) ^ref-44897

---
