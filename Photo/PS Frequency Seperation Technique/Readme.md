# Frequency Sepeartion Technique done right

Mostly from [Tutorial](http://phlearn.com/amazing-power-frequency-separation-retouching-photoshop)

This are just short notes.

lowfreq:
--------

layer -> gaussian blur (nur bis sich die farben vermischen)

highfreq:
---------
-> new layer (stamp layer) -> apply image:
    - 8bit:choose Subtract and for Scale:2, Offset:128
    - 16bit: invert on, blending add, scale 2, offset 0
-> overlay: linear light


low ist für color, high für texture
