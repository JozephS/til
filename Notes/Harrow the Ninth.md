---
kindle-sync:
  bookId: '804'
  title: Harrow the Ninth (The Locked Tomb Series Book 2)
  author: Tamsyn Muir
  asin: B07WYSGHC7
  lastAnnotatedDate: '2021-04-20'
  bookImageUrl: 'https://m.media-amazon.com/images/I/81wVk-n7G7S._SY160.jpg'
  highlightsCount: 1
---
# Harrow the Ninth
## Metadata
* Author: [Tamsyn Muir](https://www.amazon.com/Tamsyn-Muir/e/B07B3X5H5H/ref=dp_byline_cont_ebooks_1)
* ASIN: B07WYSGHC7
* ISBN: 125031321X
* Reference: https://www.amazon.com/dp/B07WYSGHC7
* [Kindle link](kindle://book?action=open&asin=B07WYSGHC7)

## Highlights
Dominus illuminatio mea et salus mea, quem timebo? — location: [5694](kindle://book?action=open&asin=B07WYSGHC7&location=5694) ^ref-5282

---
