---
kindle-sync:
  bookId: '24423'
  title: >-
    The Picture of Dorian Gray (Wisehouse Classics - with original illustrations
    by Eugene Dété)
  author: Oscar Wilde
  asin: B019AVKTPU
  lastAnnotatedDate: '2017-03-21'
  bookImageUrl: 'https://m.media-amazon.com/images/I/81CN1fxMEtL._SY160.jpg'
  highlightsCount: 1
---
# The Picture of Dorian Gray
## Metadata
* Author: [Oscar Wilde](https://www.amazon.com/Oscar-Wilde/e/B000AQ0DXI/ref=dp_byline_cont_ebooks_1)
* ASIN: B019AVKTPU
* ISBN: 978-9176371145
* Reference: https://www.amazon.com/dp/B019AVKTPU
* [Kindle link](kindle://book?action=open&asin=B019AVKTPU)

## Highlights
It is only shallow people who do not judge by appearances. — location: [427](kindle://book?action=open&asin=B019AVKTPU&location=427) ^ref-25180

---
