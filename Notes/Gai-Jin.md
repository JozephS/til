---
kindle-sync:
  bookId: '4870'
  title: 'Gai-Jin: The Third Novel of the Asian Saga'
  author: James Clavell
  asin: B00D434AFQ
  lastAnnotatedDate: '2021-07-17'
  bookImageUrl: 'https://m.media-amazon.com/images/I/91QtQvvUbNL._SY160.jpg'
  highlightsCount: 1
---
# Gai-Jin
## Metadata
* Author: [James Clavell](https://www.amazon.com/James-Clavell/e/B000APVE7S/ref=dp_byline_cont_ebooks_1)
* ASIN: B00D434AFQ
* ISBN: 0385343272
* Reference: https://www.amazon.com/dp/B00D434AFQ
* [Kindle link](kindle://book?action=open&asin=B00D434AFQ)

## Highlights
obviously, a man whose One-eyed Monk has the misfortune to be small should not be embattled with Jade Gate like that of a mare. — location: [13529](kindle://book?action=open&asin=B00D434AFQ&location=13529) ^ref-17896

---
