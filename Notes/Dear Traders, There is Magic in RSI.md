---
kindle-sync:
  bookId: '34374'
  title: >-
    Dear Traders, There is Magic in RSI: RSI Tells the Secrets, Are You
    Listening?
  author: Sudhir Dixit
  asin: B08881VCHX
  lastAnnotatedDate: '2022-01-29'
  bookImageUrl: 'https://m.media-amazon.com/images/I/713XQ2qHR6L._SY160.jpg'
  highlightsCount: 18
---
# Dear Traders, There is Magic in RSI
## Metadata
* Author: [Sudhir Dixit](https://www.amazon.com/Sudhir-Dixit/e/B088TDNWV5/ref=dp_byline_cont_ebooks_1)
* ASIN: B08881VCHX
* ISBN: B094GTYXHQ
* Reference: https://www.amazon.com/dp/B08881VCHX
* [Kindle link](kindle://book?action=open&asin=B08881VCHX)

## Highlights
The levels of RSI divergences are as follows: RSI Bearish Divergence 60-80 levels RSI Bullish Divergence 20-40 levels — location: [1040](kindle://book?action=open&asin=B08881VCHX&location=1040) ^ref-50620

Rsi level for divergence 

---
(There have been several experts including Cardwell and Hayden and several others, who claim that RSI divergences can't be traded. They failed to see that divergences can be traded and failure swing is the way to trade them.) Failure swings are rather a part of divergence and Wilder is using failure swing to confirm the divergence. Divergences can fail, if the RSI retreats from the zone, but turns back soon to enter the zone and exceed the previous high/low. Wilder is in fact saying that if failure swing is confirmed in the above manner, the risk of RSI's turning back is less and it should be expected to touch its opposite zone. — location: [1125](kindle://book?action=open&asin=B08881VCHX&location=1125) ^ref-17492

---
If RSI cannot push above the 60 level then there is weakness in the market. If RSI cannot go down below the 40 level, then there is strength in the market. If RSI retraces from 70 and remains above 50, it shows bullish trend. In fact, it indicates that it is making room for further bullish movement of price. Likewise, if RSI rises from 30 and remains below 50, it shows bearish trend. — location: [1217](kindle://book?action=open&asin=B08881VCHX&location=1217) ^ref-4002

Rsi levels

---
As per Paul Dean, anytime price is trending down the trader should be watching for Range Shifts up, and when price is trending up the trader should look for Range Shifts down. The shorter the time frame traded, the more often this will happen. — location: [1226](kindle://book?action=open&asin=B08881VCHX&location=1226) ^ref-61081

Detect dtd and trend reversal 

---
Cardwell suggests using 9 period SMA (simple moving average) and 45 period WMA (Weighted moving average) of RSI to find out the short-term and intermediate trend. If the 9 period SMA is above 45 period WMA, it is bullish. — location: [1284](kindle://book?action=open&asin=B08881VCHX&location=1284) ^ref-47526

Rsi trend detection with ma ad wma

---
Now look, RSI MA signal came much earlier and it predicted the price movement well in advance. — location: [1291](kindle://book?action=open&asin=B08881VCHX&location=1291) ^ref-35366

---
longer divergence period of weeks and even months, if using daily charts, is usually less indicative that a price detour is coming. The most powerful divergence occurs during a 2 or 3 period divergence.' According to Cardwell, number of periods between point 1 and point 2 of divergence are important. Those divergences are more powerful and strong, which come within 2-6 periods, whereas divergences found in more than 6 periods are not that significant. This is certainly correct. The nearest the divergence candles, the more powerful the signal and the swifter the price reaction. — location: [1419](kindle://book?action=open&asin=B08881VCHX&location=1419) ^ref-45349

Duration

---
The following are the most common trend reversal signals found in a candlestick chart: Morning Star (bullish) and Evening Star (bearish) Abandoned Baby Doji Star Spinning Top Hammer Bullish Engulfing and Bearish Engulfing — location: [1455](kindle://book?action=open&asin=B08881VCHX&location=1455) ^ref-17423

Candlestick

---
'If you pick up only a single notion from this book. let it be the following: When an oscillator advances or declines disproportionately to the markets' movement, you are on the wrong side of the market if you are positioned with the oscillator.' - Constance Brown — location: [1494](kindle://book?action=open&asin=B08881VCHX&location=1494) ^ref-50869

---
Positive Reversals only occur in Bullish Trends. — location: [1545](kindle://book?action=open&asin=B08881VCHX&location=1545) ^ref-32386

Remember 

---
Price Target Formula of a Positive Reversal Signal Price Target = (X-W)+Y Note: Don't get confused that X is lower than W (I know this, as I was confused at first also). You are looking at the wrong panel. See the price panel, where price at X is higher than price at W. So the formula is all right. You just have to find what the price was at W, X and Y of RSI. — location: [1595](kindle://book?action=open&asin=B08881VCHX&location=1595) ^ref-62828

Good for Bot

---
His recommendation is to get confirmation from the moving averages of RSI (not of Price). — location: [1631](kindle://book?action=open&asin=B08881VCHX&location=1631) ^ref-61615

Conform hidden divergence 

---
Cancellation: If divergence trendline is broken, it cancels out the divergence. When you get opposite signal, you book profit and exit. As Paul Dean says, 'The Positive Reversal is over as soon as the NR is confirmed. What do we do? Exit. Simple.' — location: [1635](kindle://book?action=open&asin=B08881VCHX&location=1635) ^ref-16155

Watch for opposite signals

---
'Markets tend to operate in a trading range 70-75% of the time and trend 25-30% of the time. Consequently, any moving average time period should be equally effective in a trending market and no moving average is effective in a trading range market. Conversely, oscillators that identify areas of overbought and oversold are most effective in trading range or sideways markets.'- Thomas DeMark — location: [1677](kindle://book?action=open&asin=B08881VCHX&location=1677) ^ref-18643

---
It does not work properly in a stock with very high volatility and in such cases RSI may give inconsistent signals. — location: [1690](kindle://book?action=open&asin=B08881VCHX&location=1690) ^ref-63198

---
Day traders prefer to use short periods on RSI; some use even 3 or 2 period RSI to generate more signals. — location: [1701](kindle://book?action=open&asin=B08881VCHX&location=1701) ^ref-24798

Shorter rsi

---
The most popular feigning trick is that when the professionals want to give a breakout, they just pause the buying pressure. RSI is very volatile around 70, so even with slight decrease it comes below 70, by which retail traders think that RSI is reversing and now the price is going to fall down; so some of them sell or even short sell the stock. Then the laughing professionals put the pressure on the buy pedal and kill the short-sellers by staging a breakout. — location: [1710](kindle://book?action=open&asin=B08881VCHX&location=1710) ^ref-50327

---
'At best you may achieve a 60% win rate with any strategy, including one with the RSI.' -Tradingsim.com — location: [1939](kindle://book?action=open&asin=B08881VCHX&location=1939) ^ref-51803

---
