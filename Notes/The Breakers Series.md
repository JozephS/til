---
kindle-sync:
  bookId: '57486'
  title: 'The Breakers Series: Books 1-3'
  author: Edward W. Robertson
  asin: B00E9YL3HM
  lastAnnotatedDate: '2018-12-20'
  bookImageUrl: 'https://m.media-amazon.com/images/I/81SfH51S+7L._SY160.jpg'
  highlightsCount: 1
---
# The Breakers Series
## Metadata
* Author: [Edward W. Robertson](https://www.amazon.com/Edward-W-Robertson/e/B004NW3PYY/ref=dp_byline_cont_ebooks_1)
* ASIN: B00E9YL3HM
* Reference: https://www.amazon.com/dp/B00E9YL3HM
* [Kindle link](kindle://book?action=open&asin=B00E9YL3HM)

## Highlights
He brought a chair to the back porch. She dressed, then sat outside while he trimmed her hair in careful snips. He didn't understand. Now that her wrath had gelled and cooled, she wasn't sure she could kill them all. There were so many. They had guns. Boats. Lived isolated on an island across the waters. She had to become a different Raina, but she knew too firmly what Raina looked like to do that. — location: [11528](kindle://book?action=open&asin=B00E9YL3HM&location=11528) ^ref-12087

---
