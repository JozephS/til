---
kindle-sync:
  bookId: '10068'
  title: 'The Personal MBA: Master the Art of Business'
  author: Josh Kaufman
  asin: B0046ECJ8M
  lastAnnotatedDate: '2015-09-11'
  bookImageUrl: 'https://m.media-amazon.com/images/I/51FEKUFEs7L._SY160.jpg'
  highlightsCount: 1
---
# The Personal MBA
## Metadata
* Author: [Josh Kaufman](https://www.amazon.com/Josh-Kaufman/e/B003UOZ57Q/ref=dp_byline_cont_ebooks_1)
* ASIN: B0046ECJ8M
* ISBN: 1591845572
* Reference: https://www.amazon.com/dp/B0046ECJ8M
* [Kindle link](kindle://book?action=open&asin=B0046ECJ8M)

## Highlights
will — location: [1081](kindle://book?action=open&asin=B0046ECJ8M&location=1081) ^ref-24505

---
