---
kindle-sync:
  bookId: '41846'
  title: 'Thrawn: Alliances (Star Wars) (Star Wars: Thrawn series Book 2)'
  author: Timothy Zahn
  asin: B076B51VPF
  lastAnnotatedDate: '2021-04-29'
  bookImageUrl: 'https://m.media-amazon.com/images/I/81KL5wt0v-L._SY160.jpg'
  highlightsCount: 1
---
# Thrawn
## Metadata
* Author: [Timothy Zahn](https://www.amazon.com/Timothy-Zahn/e/B000APAX8E/ref=dp_byline_cont_ebooks_1)
* ASIN: B076B51VPF
* ISBN: 1780898665
* Reference: https://www.amazon.com/dp/B076B51VPF
* [Kindle link](kindle://book?action=open&asin=B076B51VPF)

## Highlights
put down. She did so, picking a spot as far from the — location: [1192](kindle://book?action=open&asin=B076B51VPF&location=1192) ^ref-33342

---
