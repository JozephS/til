Premiere2Davinci - Speed mismatched without obvious reasons
===========================================================


One reason could be, that in Premiere somebody did set another framerate than the original clip:

1. `Right click on clip in project -> Modify -> Interpret Footage`
2. Check if framerate different
3. Set same framerate in Davinci
4. `Right click on clip in media pool -> Clip Attributes -> Video: Video Framerate`

