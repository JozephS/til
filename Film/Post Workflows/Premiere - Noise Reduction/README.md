# Noise Reduction

## Denoise II


Most Important Settings for High Noise Footage
-------------------------

[Further Reading](http://www.redgiant.com/docs/doc-on/denoiser-II/controls-finetune.php)

- Fine Tuning -> Fine Details: 0.3-0.5  # recreates artifical noise
- Advanced Settings 
    - -> Noise Hint: **HIGH**
    - -> Noise Detection Level: 20,0%
    - Shadow/Highlight Offset: adjust regarding images qualities
  
  ![Settings](https://bitbucket.org/JozephS/til/raw/0c5451a8a2da02ff60f7893e805c35b320d304fe/Film/Post%20Workflows/Noise%20Reduction/DenoiseSettings.png?at=master)
  
  
  ![Before Vs After](https://bitbucket.org/JozephS/til/raw/0c5451a8a2da02ff60f7893e805c35b320d304fe/Film/Post%20Workflows/Noise%20Reduction/BeforeVsAfter.png?at=master)

