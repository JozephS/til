LUTS & Exports in Davinci Resolve
==================

When solely working with LUT's. E.g. Client doesn't receive render, but only LUT

1) Set Data Level of your clips in Davinci Resolve to Video
2) Generate 3D LUT (CUBE) in Davinci Resolve
3) If secondary CC is needed: Export in Davinci Resolve DNxHR444 and set Export to FULL, in order to match Premiere's Prores444 clips with your generated LUT's
