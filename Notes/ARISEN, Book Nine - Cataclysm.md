---
kindle-sync:
  bookId: '30843'
  title: 'ARISEN, Book Nine - Cataclysm'
  author: Michael Stephen Fuchs
  asin: B015Y8MAK0
  lastAnnotatedDate: '2020-11-19'
  bookImageUrl: 'https://m.media-amazon.com/images/I/914KOdXzapL._SY160.jpg'
  highlightsCount: 1
---
# ARISEN, Book Nine - Cataclysm
## Metadata
* Author: [Michael Stephen Fuchs](https://www.amazon.com/Michael-Stephen-Fuchs/e/B001S2PFYA/ref=dp_byline_cont_ebooks_1)
* ASIN: B015Y8MAK0
* ISBN: 1517735777
* Reference: https://www.amazon.com/dp/B015Y8MAK0
* [Kindle link](kindle://book?action=open&asin=B015Y8MAK0)

## Highlights
senior officers die with their men.” — location: [4189](kindle://book?action=open&asin=B015Y8MAK0&location=4189) ^ref-36615

---
