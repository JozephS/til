## Remove Chromatice Aberrations


You can change color space when you are in the 'color' tab/window.
Here you can do a right mouse click on a node( the miniature clip in the upper right window.
If you do the right click on the node you can see colorspace and ser RGB is selected by default, choose YUV or LAB.
Under the colorspace menu you see also the three channels, which are enabled by default.
Declick the first channel, now you can apply a blur, most of the times, a radius of .75 is enough.

from: https://www.magiclantern.fm/forum/index.php?topic=16029.0

OR

In Resolve, load R3D into media pool. Then in Color, select Node 1 and lower Saturation to 0. Then choose Nodes>Add Layer Node, then with this new node selected slide Luma Gain to 0.01. Right Click the Layer Mixer node and choose Composite Mode "Add".
Select Layer node again, then choose Nodes>Add Serial Node. With this new Serial Node selected, go to Blur Window and make sure "Sharpen" is selected in upper right corner. Slide Radius up to taste, I went to max 1.0


OR

For CA, use a channel splitter, scale/position the individual channels (often just the red channel will do the trick), then use a combiner to put it back together. Small values in the scale will go a long way.
Scale: Alt+Click
