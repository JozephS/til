---
kindle-sync:
  bookId: '40575'
  title: >-
    Meditations - Enhanced Edition (Illustrated. Newly revised text. Includes
    Image Gallery + Audio) (Stoics In Their Own Words Book 2)
  author: Marcus Aurelius and George Long
  asin: B00IMLL63O
  lastAnnotatedDate: '2017-03-09'
  bookImageUrl: 'https://m.media-amazon.com/images/I/81RyGLaZTdL._SY160.jpg'
  highlightsCount: 2
---
# Meditations - Enhanced Edition
## Metadata
* Author: [Marcus Aurelius and George Long](https://www.amazon.comundefined)
* ASIN: B00IMLL63O
* Reference: https://www.amazon.com/dp/B00IMLL63O
* [Kindle link](kindle://book?action=open&asin=B00IMLL63O)

## Highlights
The soul of man does — location: [194](kindle://book?action=open&asin=B00IMLL63O&location=194) ^ref-40775

---
violence to itself, — location: [194](kindle://book?action=open&asin=B00IMLL63O&location=194) ^ref-46130

---
