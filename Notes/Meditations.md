---
kindle-sync:
  bookId: '36017'
  title: 'Meditations: A New Translation (Modern Library)'
  author: Marcus Aurelius and Gregory Hays
  asin: B000FC1JAI
  lastAnnotatedDate: '2020-08-15'
  bookImageUrl: 'https://m.media-amazon.com/images/I/71dwCdjTA-L._SY160.jpg'
  highlightsCount: 1
---
# Meditations
## Metadata
* Author: [Marcus Aurelius and Gregory Hays](https://www.amazon.com/Marcus-Aurelius/e/B000AR7YUW/ref=dp_byline_cont_ebooks_1)
* ASIN: B000FC1JAI
* Reference: https://www.amazon.com/dp/B000FC1JAI
* [Kindle link](kindle://book?action=open&asin=B000FC1JAI)

## Highlights
13. Nothing is more pathetic than people who run around in circles, “delving into the things that lie beneath” and conducting investigations into the souls of the people around them, never realizing that all you have to do is to be attentive to the power inside you and worship it sincerely. To worship it is to keep it from being muddied with turmoil and becoming aimless and dissatisfied with nature—divine and human. What is divine deserves our respect because it is good; what is human deserves our affection because it is like us. And our pity too, sometimes, for its inability to tell good from bad—as terrible a blindness as the kind that can’t tell white from black. — location: [975](kindle://book?action=open&asin=B000FC1JAI&location=975) ^ref-17969

---
