# Workflow with FCP->PremierePro->DaVinci->AE:

## FCP to Resolve (via Premiere)
* Get FCP.xml
* Import in PremierePro
* Locate Files
* Export to FCP.xml
* Edit pproimport.xml //?/ to //localhost/
* Import in DaVinci
* Conform/ColorGrade/Render
* Davinci: Easy: None / Render as "Target" DPX 10bit

### optional:
* Davinci:Render Easy Solution Final CutExport
* Davinci:Conform Export Timeline as XML
* Import .xml in AE with AutomaticDuck Import


## Resolve to AE
* Import DPX in AfterEffects
* Make AE Magic (Denoise, Sharpen, etc)
* Render in AE

## Finalize in Premiere
* New PremierePro-Project
* Open in PremierePro
* Add RenderedClip from AE
* Add AudioTrack again

### optional:
* Export to/Import in AfterEffects (depends if you want one track or several clips in AE)
