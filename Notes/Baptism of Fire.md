---
kindle-sync:
  bookId: '12141'
  title: 'Baptism of Fire: Witcher 3 – Now a major Netflix show (The Witcher)'
  author: Andrzej Sapkowski and David French
  asin: B00GFHFRSM
  lastAnnotatedDate: '2016-12-09'
  bookImageUrl: 'https://m.media-amazon.com/images/I/81ZK4D+MPKL._SY160.jpg'
  highlightsCount: 1
---
# Baptism of Fire
## Metadata
* Author: [Andrzej Sapkowski and David French](https://www.amazon.com/Andrzej-Sapkowski/e/B001ICAMAW/ref=dp_byline_cont_ebooks_1)
* ASIN: B00GFHFRSM
* ISBN: 0575090960
* Reference: https://www.amazon.com/dp/B00GFHFRSM
* [Kindle link](kindle://book?action=open&asin=B00GFHFRSM)

## Highlights
don’t ask the questions, riffraff! You answer them!’ The — location: [2757](kindle://book?action=open&asin=B00GFHFRSM&location=2757) ^ref-37565

---
