---
kindle-sync:
  bookId: '19199'
  title: 'Rest: Why You Get More Done When You Work Less'
  author: Alex Soojung-Kim Pang
  asin: B06XKQ32J7
  lastAnnotatedDate: '2022-02-15'
  bookImageUrl: 'https://m.media-amazon.com/images/I/81p-up+P9BL._SY160.jpg'
  highlightsCount: 12
---
# Rest
## Metadata
* Author: [Alex Soojung-Kim Pang](https://www.amazon.com/Alex-Soojung-Kim-Pang/e/B001KHJ91I/ref=dp_byline_cont_ebooks_1)
* ASIN: B06XKQ32J7
* ISBN: 0465074871
* Reference: https://www.amazon.com/dp/B06XKQ32J7
* [Kindle link](kindle://book?action=open&asin=B06XKQ32J7)

## Highlights
STUDIES OF THE default mode network and mind-wandering help us make sense of a phenomenon that has long puzzled psychologists. Many famous stories of problem-solving or creative breakthroughs begin with a period of intense work and focus, during which the scientist or artist or writer pores over evidence, labors over theories, and struggles toward an answer. Frustrated and tired, she stops for a break and turns her attention to something else. Days or weeks later, a solution suddenly appears; she hadn’t been thinking about the problem, but in a flash, the answer is suddenly present in her mind, as clear as day. She then returns to the problem and verifies that the insight is correct. This is a model described by English psychologist Graham Wallas in his 1926 book The Art of Thought. After studying accounts of creative breakthroughs and moments of insight, Wallas concluded that they follow a four-stage process. — location: [629](kindle://book?action=open&asin=B06XKQ32J7&location=629) ^ref-14048

---
After his morning walk and breakfast, Darwin was in his study by eight and worked a steady hour and a half. At nine thirty he would read the morning mail and write letters. Downe was far away enough from London to discourage casual visitors, yet close enough to allow the morning mail to reach correspondents and colleagues in the city in just a few hours. At ten thirty, Darwin returned to more serious work, sometimes moving to his aviary, greenhouse, or one of several other buildings where he conducted his experiments. By noon, he would declare, “I’ve done a good day’s work,” and set out on a long walk on the Sandwalk, a path he had laid out not long after buying Down House. (Part of the Sandwalk ran through land leased to Darwin by the Lubbock family.) When he returned after an hour or more, Darwin had lunch and answered more letters. At three he would retire for a nap; an hour later he would arise, take another walk around the Sandwalk, then return to his study until five thirty, when he would join his wife, Emma, and their family for dinner. On this schedule he wrote nineteen books, including technical volumes on climbing plants, barnacles, and other subjects; the controversial Descent of Man; and The Origin of Species, probably the single most famous book in the history of science, and a book that still affects the way we think about nature and ourselves. Anyone who reviews his schedule cannot help but notice the creator’s paradox. Darwin’s life revolved around science. Since his undergraduate days, Darwin had devoted himself to scientific collecting, exploration, and eventually theorizing. He and Emma moved to the country from London to have more space to raise a family and to have more space—in more than one sense of the word—for science. Down House gave him space for laboratories and greenhouses, and the countryside gave him the peace and quiet necessary to work. But at the same time, his days don’t seem very busy to us. The times we would classify as “work” consist of three ninety-minute periods. If he had been a professor in a university today, he would have been denied tenure. If he’d been working in a company, he would have been fired within a week. — location: [719](kindle://book?action=open&asin=B06XKQ32J7&location=719) ^ref-3174

---
day that starts with work creates rest that can be enjoyed without guilt. When you start early, the rest you take is the rest you’ve earned. — location: [1100](kindle://book?action=open&asin=B06XKQ32J7&location=1100) ^ref-3386

---
Under normal circumstances, you reach peak alertness around 8 a.m. and 8 p.m.; — location: [1530](kindle://book?action=open&asin=B06XKQ32J7&location=1530) ^ref-4734

Research

---
and there’s some evidence that sleeping well in middle age provides some insurance against dementia later in life. — location: [1880](kindle://book?action=open&asin=B06XKQ32J7&location=1880) ^ref-57085

---
Many of them are dedicated athletes: they find that exercise provides a break from work, strengthens the physical foundations of creative performance, and—as scientists have recently discovered—keeps their brains healthy. Deep play—hobbies that are challenging, mentally absorbing, and personally meaningful—provide another important source of recovery. Finally, sabbaticals give — location: [2175](kindle://book?action=open&asin=B06XKQ32J7&location=2175) ^ref-54956

---
behavioral, and environmental factors affect things like aging, intelligence, and success; in studies comparing identical twins, genetics ceases to be a factor. The researchers administered psychological, neurological, and health and fitness — location: [2385](kindle://book?action=open&asin=B06XKQ32J7&location=2385) ^ref-17910

---
We have to choose to make an earlier start to the day to earn time to rest later; we have to reserve space on the daily calendar for a walk, or keep time free on the weekends for a hobby or sport; we must arrange our finances and business affairs so we can take a sabbatical. When — location: [2982](kindle://book?action=open&asin=B06XKQ32J7&location=2982) ^ref-50021

---
The very act of making specific plans helps make a goal feel more realistic and accessible, and gives you a clearer sense of its value. — location: [2987](kindle://book?action=open&asin=B06XKQ32J7&location=2987) ^ref-45524

---
Protecting time for rest also forces you to consider whether a new opportunity, request for a favor, or demand on your time is really worth it. It helps you identify tasks that you might casually accept and regret later, and gives you permission to (diplomatically) turn them down. It — location: [2991](kindle://book?action=open&asin=B06XKQ32J7&location=2991) ^ref-2845

---
For most of history, leaders were supposed to appear calm and unhurried; success began with self-mastery and self-control. As early as the sixth century BCE (before Plato and Aristotle), Chinese general Sun Tzu wrote in The Art of War, “It is the unemotional, reserved, calm, detached warrior who wins, not the hothead seeking vengeance and not the ambitious seeker of fortune.” — location: [3006](kindle://book?action=open&asin=B06XKQ32J7&location=3006) ^ref-34231

---
The Distraction Addiction: Getting the Information You Need and the Communication You Want, Without Enraging Your Family, Annoying Your Colleagues, and Destroying Your Soul — location: [3274](kindle://book?action=open&asin=B06XKQ32J7&location=3274) ^ref-23897

Buy

---
