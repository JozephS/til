---
kindle-sync:
  bookId: '51777'
  title: Choose Yourself!
  author: James Altucher and Dick Costolo
  asin: B00CO8D3G4
  lastAnnotatedDate: '2015-12-11'
  bookImageUrl: 'https://m.media-amazon.com/images/I/81Q89FCTzjL._SY160.jpg'
  highlightsCount: 2
---
# Choose Yourself!
## Metadata
* Author: [James Altucher and Dick Costolo](https://www.amazon.com/James-Altucher/e/B001IOBN80/ref=dp_byline_cont_ebooks_1)
* ASIN: B00CO8D3G4
* Reference: https://www.amazon.com/dp/B00CO8D3G4
* [Kindle link](kindle://book?action=open&asin=B00CO8D3G4)

## Highlights
is an inspiration to you. X) — location: [644](kindle://book?action=open&asin=B00CO8D3G4&location=644) ^ref-65300

---
amazing how many people wrote long, gushing — location: [1786](kindle://book?action=open&asin=B00CO8D3G4&location=1786) ^ref-41793

---
