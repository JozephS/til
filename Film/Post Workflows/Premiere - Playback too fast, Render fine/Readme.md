Playback Speed too fast
======================

Issue:
--------
Playback in Premiere is to fast/laggy but when rendered it is fine


Solutions:
------------

1.  Check fps settings between timelines/source

Fps should be the same

2. Check audio hardware setting in premiere

- Input/Output should be same/reliable device
- I adjusted my latency to 100ms from 200ms (works for me)


Should look like this:

![Audio Hardware](https://bytebucket.org/JozephS/til/raw/71c5e704826ed7fd055dd52db465e7d93529e945/Film/Post%20Workflows/Premiere%20-%20Playback%20to%20fast%2C%20Render%20fine/AudioHardware.png)
