---
kindle-sync:
  bookId: '27723'
  title: 'JUMP: A Futuristic Fantasy (Book 1) (THE FALLEN Dark Fantasy Series)'
  author: Steve Windsor
  asin: B00NN6LI6S
  lastAnnotatedDate: '2015-12-02'
  bookImageUrl: 'https://m.media-amazon.com/images/I/91XJaLAlrRL._SY160.jpg'
  highlightsCount: 4
---
# JUMP
## Metadata
* Author: [Steve Windsor](https://www.amazon.com/Steve-Windsor/e/B00D8V7HRW/ref=dp_byline_cont_ebooks_1)
* ASIN: B00NN6LI6S
* ISBN: 0692291466
* Reference: https://www.amazon.com/dp/B00NN6LI6S
* [Kindle link](kindle://book?action=open&asin=B00NN6LI6S)

## Highlights
helped her endure his insolence. — location: [581](kindle://book?action=open&asin=B00NN6LI6S&location=581) ^ref-54257

---
the — location: [1467](kindle://book?action=open&asin=B00NN6LI6S&location=1467) ^ref-37698

---
it, remember? So don’t go giving me any of that “beating up on women” shit, — location: [2351](kindle://book?action=open&asin=B00NN6LI6S&location=2351) ^ref-7077

---
roost — location: [3771](kindle://book?action=open&asin=B00NN6LI6S&location=3771) ^ref-41785

---
