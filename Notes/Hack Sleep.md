---
kindle-sync:
  bookId: '2477'
  title: >-
    Hack Sleep: How to Fall Asleep Faster, Sleep Better and Sleep Well, and
    Naturally Reverse Sleep Disorders (Hacks to Create a New Future Book 4)
  author: Danny Flood
  asin: B01674BNU8
  lastAnnotatedDate: '2020-02-25'
  bookImageUrl: 'https://m.media-amazon.com/images/I/61AMqgTj7BL._SY160.jpg'
  highlightsCount: 2
---
# Hack Sleep
## Metadata
* Author: [Danny Flood](https://www.amazon.com/Danny-Flood/e/B00SOQXFEU/ref=dp_byline_cont_ebooks_1)
* ASIN: B01674BNU8
* Reference: https://www.amazon.com/dp/B01674BNU8
* [Kindle link](kindle://book?action=open&asin=B01674BNU8)

## Highlights
explains in this video (skip to 0:34). You can — location: [231](kindle://book?action=open&asin=B01674BNU8&location=231) ^ref-29842

Note

---
hours. — location: [253](kindle://book?action=open&asin=B01674BNU8&location=253) ^ref-57697

---
