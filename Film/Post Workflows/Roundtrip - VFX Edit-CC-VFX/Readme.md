Workflow for VFX: Use Case: Editor->CC->VFX:
=============================================

1. Edit in Premiere/Final Cut(yuk)/whatever
2. Export Clips with **VFX** to transport format (like **dnxhd**)
3. Reimport this clips to Timeline
4. a) Send Clips to VFX
4. b) Send Timeline (e.g. Final Cut XML) to CC
5. To CC, when VFX done, just import/replace them in CC Suite (e.g. Edit in Resolve)

