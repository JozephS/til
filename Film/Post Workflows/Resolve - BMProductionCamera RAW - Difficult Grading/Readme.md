# BMD RAW

## Intro

In one of our last projects with had a little bit of trouble of bringing the color of a particular shot up to par with the rest. There was another one with similar but not so heavy problems. Characteristics of this shot included that they are more exposed to the left than usual (because of strong highlights) and so the contrast waves were very thin. Our first approach was to split it up via adding a LUT Node, add a Node before it to set contrast, pivot, the usual jazz and then append a Node with ColorChart Infos for good measure. Sadly the result were less than optimal. Then we tried this “counter-intuitive” approach:

## Files

I also include the original dpx so you can check out what we did in particular for this shot. But don’t forget to also the step-by-step described below in **Setup**.

##Setup

![Original Pic](./original.jpg)

1) Use LUT in Preferences, not in **Node** 

![Add LUT in Preferences](./step-by-step.jpg)

2) Looks already better

![With LUT](./onlylut.jpg)

3) NOW add Contrast, Pivot, Lift, Gamma, etc

![FIXED](./fixed.jpg)

