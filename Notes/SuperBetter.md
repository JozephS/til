---
kindle-sync:
  bookId: '58417'
  title: 'SuperBetter: The Power of Living Gamefully'
  author: Jane McGonigal
  asin: B00SI02DHC
  lastAnnotatedDate: '2020-06-01'
  bookImageUrl: 'https://m.media-amazon.com/images/I/81ut3nQv0JL._SY160.jpg'
  highlightsCount: 17
---
# SuperBetter
## Metadata
* Author: [Jane McGonigal](https://www.amazon.com/Jane-McGonigal/e/B003W4XJIG/ref=dp_byline_cont_ebooks_1)
* ASIN: B00SI02DHC
* ISBN: 1594206368
* Reference: https://www.amazon.com/dp/B00SI02DHC
* [Kindle link](kindle://book?action=open&asin=B00SI02DHC)

## Highlights
One recent study showed that gamers exhibited “a dispositional need to complete difficult tasks” and “the desire to exhibit high standards of performance in the face of frustration.”13 When — location: [1577](kindle://book?action=open&asin=B00SI02DHC&location=1577) ^ref-10969

---
Work ethic is not a moral virtue that can be cultivated simply by wanting to be a better person. It’s actually a biochemical condition that can be fostered, purposefully, through activity that increases dopamine levels in the brain. This explains precisely how challenging video games—like Re-Mission—could prime us to tackle other everyday challenges with higher effort and more determination. — location: [1588](kindle://book?action=open&asin=B00SI02DHC&location=1588) ^ref-38459

---
Indeed, Dr. Bavelier has identified video games as potentially the single most effective intervention for increasing neuroplasticity in adults. — location: [1605](kindle://book?action=open&asin=B00SI02DHC&location=1605) ^ref-31518

---
It expects to learn and improve and eventually succeed, because that’s what it’s used to doing. — location: [1620](kindle://book?action=open&asin=B00SI02DHC&location=1620) ^ref-4182

---
Fifteen years’ worth of neuroscience research on games adds up to one big idea: if you want to change your brain for the better—to turn motivation into self-efficacy, to learn faster, and to cultivate more resilience—play more games. Or at the very least, provoke your brain with challenging learning opportunities in the same ways that good games do. — location: [1626](kindle://book?action=open&asin=B00SI02DHC&location=1626) ^ref-20608

---
“Worst-case-scenario bingo” may not be a game you look forward to playing—because really, who wants to be in a stressful or unpleasant situation? But if you do need to tap into your determination and grit, this gameful intervention is a brilliant way to prepare your brain for resilience and success. — location: [1691](kindle://book?action=open&asin=B00SI02DHC&location=1691) ^ref-56068

---
While you’re at it, why not create a “best-case-scenario” bingo card for your next big day? Think of all the good things that could happen to you on a trip, or at a big work event, or on a special occasion. After all, you can benefit from determination and optimism on fun days just as much as on tough ones! — location: [1693](kindle://book?action=open&asin=B00SI02DHC&location=1693) ^ref-34363

---
I’m eager to tackle this obstacle. — location: [2420](kindle://book?action=open&asin=B00SI02DHC&location=2420) ^ref-36621

---
I get fired up when I think about tackling this obstacle. — location: [2426](kindle://book?action=open&asin=B00SI02DHC&location=2426) ^ref-29888

---
13. I get excited when I think about the possible outcomes of tackling this obstacle. — location: [2431](kindle://book?action=open&asin=B00SI02DHC&location=2431) ^ref-25785

---
I don’t mind struggling with this obstacle, or sometimes failing, because the outcome is important to me. — location: [2432](kindle://book?action=open&asin=B00SI02DHC&location=2432) ^ref-63371

---
I think I have or can acquire the abilities needed to successfully tackle this obstacle. — location: [2433](kindle://book?action=open&asin=B00SI02DHC&location=2433) ^ref-2940

---
16. If I succeed, my choosing to tackle this obstacle will have a positive impact on my or my family’s health and happiness. — location: [2434](kindle://book?action=open&asin=B00SI02DHC&location=2434) ^ref-3881

---
I’ll probably learn something by tackling this obstacle as best I can. — location: [2439](kindle://book?action=open&asin=B00SI02DHC&location=2439) ^ref-36458

---
Do ten push-ups even if I’m exhausted—in fact, especially if I’m exhausted, because I like how strong it makes me feel. I think to myself: Screw you, exhaustion! Look at what I can do! I call them screw-you push-ups, and they feel awesome. (Confession: I just did a set, to fight writer’s block!) What do all these power-ups have in common? I can do them easily, at no cost, and they never fail to make me feel at least a little bit better, no matter what else I’m thinking or feeling or battling that day. — location: [2746](kindle://book?action=open&asin=B00SI02DHC&location=2746) ^ref-63483

---
It’s okay to feel depressed or anxious sometimes. I take action on a problem, even when I fear I may fail or get it wrong. I don’t avoid situations that make me feel nervous. It’s okay if I remember something unpleasant. I can move toward important goals, even if I don’t feel good about myself. I don’t have to get rid of every scary or upsetting image that comes to my mind. I would rather achieve my goals than avoid unpleasant thoughts and feelings. — location: [3370](kindle://book?action=open&asin=B00SI02DHC&location=3370) ^ref-40079

---
Do you really need to feel calm, or rested, or pain-free to pursue your goals? This is the single most powerful kind of psychological flexibility you can achieve. — location: [3470](kindle://book?action=open&asin=B00SI02DHC&location=3470) ^ref-36480

---
