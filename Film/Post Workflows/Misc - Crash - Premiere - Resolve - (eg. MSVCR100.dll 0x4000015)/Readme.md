# Resolve & Adobe Premiere, Media Encoder Troubles

Errors, Crashes, CTD, BlueScreen,
eg.:

>   Faulting application name: Adobe Media Encoder.exe, version: 6.0.3.1, time stamp: 0x5057aebb
>   Faulting module name: MSVCR100.dll, version: 10.0.40219.325, time stamp: 0x4df2be1e
>   Exception code: 0x40000015
>   Fault offset: 0x0008d6fd
>   Faulting process id: 0x3c24
>   Faulting application start time: 0x01d1883515baa4c6
>   Faulting application path: C:\Program Files (x86)\Adobe\Adobe Media Encoder CS6\Adobe Media Encoder.exe
>   Faulting module path: C:\Windows\SYSTEM32\MSVCR100.dll
>   Report Id: 9bc40c89-5cd6-4811-ab95-16350012592c
>   Faulting package full name: 
>   Faulting package-relative application ID: 


# TroubleShoot Steps
 * Reinstall msvcr via redistributal 2010 (x64 and x86)
 * disable all graphic cards except one
 * in nvidia control settings, enable cuda on only main card/all cards
 * in nvidia control settings, seperatly set cuda devices for resolve/premiere/afterfx/media encoder/etc
