---
kindle-sync:
  bookId: '13669'
  title: >-
    The $100 Startup: Reinvent the Way You Make a Living, Do What You Love, and
    Create a New Future
  author: Chris Guillebeau
  asin: B0067TGSOK
  lastAnnotatedDate: '2020-01-17'
  bookImageUrl: 'https://m.media-amazon.com/images/I/81uwcWbfnlL._SY160.jpg'
  highlightsCount: 3
---
# The $100 Startup
## Metadata
* Author: [Chris Guillebeau](https://www.amazon.com/Chris-Guillebeau/e/B003G218QO/ref=dp_byline_cont_ebooks_1)
* ASIN: B0067TGSOK
* ISBN: 0307951529
* Reference: https://www.amazon.com/dp/B0067TGSOK
* [Kindle link](kindle://book?action=open&asin=B0067TGSOK)

## Highlights
“If you make your business about helping others, you’ll always have plenty of work.” Here’s — location: [656](kindle://book?action=open&asin=B0067TGSOK&location=656) ^ref-25734

---
set up shop in a Seattle farmer’s market with a sign that read “5-Cent Architecture Advice.” — location: [2157](kindle://book?action=open&asin=B0067TGSOK&location=2157) ^ref-50251

1€ - webdev advise (auf freelancer.org)

---
The One-Page Promotion Plan — location: [2227](kindle://book?action=open&asin=B0067TGSOK&location=2227) ^ref-35145

---
