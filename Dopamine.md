Dopamine low levels responsible for low motivation and not enjoying => "Mean Gene"

Naturally increase low level:
- Rollercoaster (VR)
- Risk and fear (VR?)
- protein-rich: turkey, beef, eggs, dairy, soy, and legumes 
- Eat less saturated fat
- Consume probiotics
- Eat velvet beans (aka Mucuna pruriens) TOXIC or [Fava Beans](https://www.healthline.com/nutrition/fava-beans)
- **Exercise often**
- Get enough sleep
- **Listen to music**
- Meditate
- enough sunlight
- Vitamin supplements



"10 Best Ways to Increase Dopamine Levels Naturally" https://www.healthline.com/nutrition/how-to-increase-dopamine#TOC_TITLE_HDR_3

### Eat lots of protein
Proteins are made up of smaller building blocks called amino acids.

About 20 different amino acids are needed to make all the proteins in your body. Your body can make some of these amino acids, and you must get the others from food (5Trusted Source).

One amino acid called tyrosine plays a critical role in the production of dopamine (6Trusted Source).

Enzymes within your body can turn tyrosine into dopamine, so having adequate tyrosine levels is important for dopamine production.

Tyrosine can also be made from another amino acid called phenylalanine (6Trusted Source).

Both tyrosine and phenylalanine are naturally found in protein-rich foods such as turkey, beef, eggs, dairy, soy, and legumes (7Trusted Source, 8Trusted Source).

**While these studies show that extremely high or extremely low intakes of these amino acids can impact dopamine levels, it’s unknown whether normal variations in protein intake would have much impact**.


### Exercise often

Exercise is recommended for boosting endorphin levels and improving mood.

Improvements in mood can be seen after as little as 10 minutes of aerobic activity but tend to **be highest after at least 20 minutes** (32Trusted Source).

While these effects are probably not entirely due to changes in dopamine levels, animal research suggests that exercise can boost dopamine levels in the brain.

In rats, treadmill running increases the release of dopamine and upregulates the number of dopamine receptors in the reward areas of the brain (33Trusted Source).

However, one 3-month study in humans found that **performing 1 hour of yoga 6 days per week significantly increased dopamine levels (34Trusted Source).**


### Music

A small 2011 study investigating the effects of music on dopamine found a 9% increase in brain dopamine levels when people listened to instrumental songs that gave them chills (44Trusted Source).

Because music can boost dopamine levels, listening to music has even been shown to help people with Parkinson’s disease improve their fine motor control (45Trusted Source).

#### SUMMARY
Listening to your favorite instrumental and choral music may boost your dopamine levels.

### Meditate
Meditation is the practice of clearing your mind, focusing inward, and letting your thoughts float by without judgment or attachment.

You can do it while standing, sitting, or even walking, and regular practice is associated with improved mental and physical health (46Trusted Source, 47Trusted Source).

New research has found that these benefits may be due to increased dopamine levels in the brain.

One study including 8 experienced meditation teachers found a 65% increase in dopamine production after meditating for 1 hour, compared with resting quietly (48Trusted Source).

These changes are thought to help meditators maintain a positive mood and stay motivated to remain in the meditative state for longer (49Trusted Source).

However, it’s unclear whether these dopamine-boosting effects happen only in experienced meditators or occur in people who are new to meditation as well.

##### SUMMARY
Meditation increases dopamine levels in the brains of experienced meditators, but it’s unclear whether these effects also occur in those who are new to meditation.