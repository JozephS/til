---
kindle-sync:
  bookId: '38073'
  title: >-
    The Now Habit: A Strategic Program for Overcoming Procrastination and
    Enjoying Guilt-Free Play
  author: Neil A. Fiore
  asin: B001QNVP7M
  lastAnnotatedDate: '2019-03-10'
  bookImageUrl: 'https://m.media-amazon.com/images/I/715qPAQ6OBL._SY160.jpg'
  highlightsCount: 5
---
# The Now Habit
## Metadata
* Author: [Neil A. Fiore](https://www.amazon.com/Neil-A-Fiore/e/B000APJK5Q/ref=dp_byline_cont_ebooks_1)
* ASIN: B001QNVP7M
* Reference: https://www.amazon.com/dp/B001QNVP7M
* [Kindle link](kindle://book?action=open&asin=B001QNVP7M)

## Highlights
The fear of judgment is the key fear that stems from over-identifying who you are, your worth as a person, with your work. From this fear follows the counterproductive drive toward perfectionism, severe self-criticism, and the fear that you must deprive yourself of leisure time in order to satisfy some unseen judge. — location: [306](kindle://book?action=open&asin=B001QNVP7M&location=306) ^ref-2788

---
Whenever you catch yourself losing motivation on a project, look for the implicit “have to” in your thinking and make a decision at that moment to embrace the path—as it is, not the way you think it should be—or let go of it. It’s your choice. — location: [1014](kindle://book?action=open&asin=B001QNVP7M&location=1014) ^ref-61581

---
The language, attitudes, and behaviors of producers can be acquired through specific, on-the-job practice. For example, if you’re at your desk looking at a pile of unanswered mail and a list of unreturned telephone calls, the first thing you may notice is that your shoulders begin to droop forward in a depressed, burdened fashion. This is a clear signal that, even if you haven’t heard yourself say, “I have to,” you feel victimized rather than responsible and powerful. At that moment of awareness, immediately choose to work or accept responsibility for choosing to delay. Use your awareness of a negative thought or attitude to reflexively shift you to the producer’s attitude of choice and power. — location: [1048](kindle://book?action=open&asin=B001QNVP7M&location=1048) ^ref-956

---
“I choose to start on one small step, knowing I have plenty of time for play.” — location: [1110](kindle://book?action=open&asin=B001QNVP7M&location=1110) ^ref-24684

---
Studies have confirmed that as little as thirty seconds of staying with a feared situation—a barking dog, a crowded party, giving a speech—while using positive self-talk is enough to start the process of replacing a phobic response with positive alternatives. — location: [1386](kindle://book?action=open&asin=B001QNVP7M&location=1386) ^ref-3851

Anna

---
