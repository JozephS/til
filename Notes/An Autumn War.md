---
kindle-sync:
  bookId: '35524'
  title: 'An Autumn War: The Long Price Quartet'
  author: Daniel Abraham
  asin: B003K15OLQ
  lastAnnotatedDate: '2015-09-21'
  bookImageUrl: 'https://m.media-amazon.com/images/I/518xi9UROdL._SY160.jpg'
  highlightsCount: 1
---
# An Autumn War
## Metadata
* Author: [Daniel Abraham](https://www.amazon.com/Daniel-Abraham/e/B001H6RS5Q/ref=dp_byline_cont_ebooks_1)
* ASIN: B003K15OLQ
* ISBN: 0765313421
* Reference: https://www.amazon.com/dp/B003K15OLQ
* [Kindle link](kindle://book?action=open&asin=B003K15OLQ)

## Highlights
the emptiness — location: [567](kindle://book?action=open&asin=B003K15OLQ&location=567) ^ref-34878

---
