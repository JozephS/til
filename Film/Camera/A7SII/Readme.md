#A7S II best practices and pitfalls for filming

## Best Practice
### To Avoid Highlight Banding Issues:

Workaround:
----------
Set *PP6* to Cine4->S-Gamut3.Cine instead of sLog

Settings to bring material to (near) REC709 
-------------------------------------------
so you can use other filter/looks calibrated for rec709

**FPX:**
- Global: +22%
- Shadows: +8%
- Midtones: +9%
- Highlights: -37%

**Davinci:**
- 30.5 offset
- 0.08 lift
- 0.09 gamma
- 0.63 gain


[Article here](http://www.marienho.com/how-to-use-slog3-luts-for-the-sony-a7s-ii-without-banding-in-the-highlights/)

### Expose to the Right (ETTR) for max useable data

may make blacks? I think the reverse is true. If you are too dark, blacks are getting noise because you have to lift them.

see [Article](http://www.redsharknews.com/production/item/2257-exposing-to-the-right-on-the-sony-a7s-and-how-to-grade-the-footage)

##Setup for Shoot

- PP6: Cine4->S-Gamut3.Cine
- Reduce Details (**hard to focus on set, but can sharp better in post**) *had mixed results with this*
- Saturation: 0

## Other Resource
- [SLOG2 Explained](http://www.xdcam-user.com/2014/08/exposing-and-using-slog2-on-the-sony-a7s-part-one-gamma-and-exposure/)
- [Workflows Discussion](http://nofilmschool.com/boards/discussions/sony-a7s-ii-slog3-s-gamut3-noise-issue-test-footage)
- [LUT](http://www.colorgradingcentral.com/impulz)
- [LUT](http://www.eoshd.com/comments/topic/7776-3d-luts-for-a7s-slog2-and-sgamut/)
- *Our own LUT Archive*: see folder “LUT” in this repo



