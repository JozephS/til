# EQ Rental Best-practices & Todos

Stuff I learned and forgot again over the years while renting EQ


## Todos

[ ] Sensor: check for scratches & take photos
[ ] Lenses: check for scratches & take photos
[ ] Lenses: Always transport wide-open F and focus on horizon
[ ] Chargers: get chargers for everything
[ ] Cardreader: for every format
[ ] Insurance: Do you have it?


### Flights:

[ ] Check weight for carry-on luggage: 8-24kg 
[ ] Bring enough "bags", just for emergency
[ ] Lenses: Always transport wide-open F and focus on horizon


