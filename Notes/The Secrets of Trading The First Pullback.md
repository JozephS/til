---
kindle-sync:
  bookId: '10119'
  title: >-
    The Secrets of Trading The First Pullback: A Price Action Guide For
    Understanding Market Pullback That Works
  author: Alwin Ng
  asin: B00JTU7HNK
  lastAnnotatedDate: '2021-06-10'
  bookImageUrl: 'https://m.media-amazon.com/images/I/81vaWb7KI8L._SY160.jpg'
  highlightsCount: 2
---
# The Secrets of Trading The First Pullback
## Metadata
* Author: [Alwin Ng](https://www.amazon.com/Alwin-Ng/e/B00E99VODA/ref=dp_byline_cont_ebooks_1)
* ASIN: B00JTU7HNK
* ISBN: 1511779349
* Reference: https://www.amazon.com/dp/B00JTU7HNK
* [Kindle link](kindle://book?action=open&asin=B00JTU7HNK)

## Highlights
Three Advancing White Soldiers. — location: [1078](kindle://book?action=open&asin=B00JTU7HNK&location=1078) ^ref-41186

---
These clues include: -         Shallow Pullbacks -         Trend Bars -         Price Channels -         Horizontal Support and Resistance level -         Knowing the market players and timeframe — location: [1179](kindle://book?action=open&asin=B00JTU7HNK&location=1179) ^ref-16485

---
