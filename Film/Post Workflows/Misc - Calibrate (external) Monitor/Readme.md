DISPLAYDECK: SAMSUNG NU7199 TV
----------------

Calibrated exactly via https://hub.displaycal.net/wiki/3d-lut-creation-workflow-for-resolve/ for TV


# LUT (Resolve)

`Resolve-SamsungNU7199-HDMI-colorspace_rec709-tonecurve_rec1886-140cdm-18-12-21--01'49''.Rec709.B0.0,2.4Gawn65.cube`

## Bildeinstellungen am TV

(Interactive Displayconfiguration)

- Bildmodus: Standard
- Einstellung der Bildgrösse: Benutzerdefiniert

### Experten Einstellung

- Hintergrundbeleuchtung: 29
- Helligkeit: 0
- Kontrast: 50
- Schärfe: 10
- Farbe: 50
- Farbton (G/R): 0
- Bildeinstellung anwenden: Aktuelle Quelle
- Digital aufbereiten: aus
- Auto Motion Plus: aus
- Kontrastverbesserung: Aus
- Farbton: Standard
- Weissabgleich (2Punkte): Gain (R/G/B): 30/0/-35, Offset (R/G/B): 0/0/0
- Gamma: BT.1886 / 0
- Nur RGB-Modus: aus
- Farbraumeinstellungen: Benutzerdefiniert (R/G/B): 50/0/0


WINDOWS: SAMSUNG U32D970  
----------------

sRGB, 140cdm

# Monitor Color Mode
- Calibration 1 (sRGB)

# Windows Color Management

`U32D970-sRGB-140cdm-18-12-14--12'31''.icm`

# LUT (Resolve/Viewer)
`U32D970-sRGB-140cdm-18-12-14--12'31''.Rec709.B0.0,2.4eGawn65.cube`



DECKLINK: SAMSUNG U32D970  
----------------

Trick dabei: Monitor vorher im auf Calibration 3 stellen, damit der Farbraum maximal ist für rec709 im Videomodus
rec709, 140cdm

# Monitor Calibration
- Windows: Calibration 3 (AdobeRGB)
- Switch HDMI Eingang: Custom

### CUSTOM 
- PC/AV Modus: AV

# Bildeinstellungen am TV
(Interactive Displayconfiguration)
## Picture
- Color Mode: Custom
- Brigthness: 53
- Contrast: 75
- Sharpness: 60
- Color: R/G/B: 50/50/50 (optimum/optimum)
- Response Time: Standard
- HDMI Black Level: Low


# LUT (Resolve/Video)
`Resolve-AdobeRGBCalib3-HDMI-Custom-rec709-140cdm-18-12-14--14'53''.Rec709.B0.0,2.4Gawn65`


=====

AKSEL MONITOR ANGLEICH
---------------------------

- Monitor: auf rec709 kalibrietes Profil eingestellt
- Windows Color Profile: mit Samsung Natural Color Expert, DeepBlack  `FILE: U32D970_Calibration3_REC709_DeepBlack_160.0cd_6504K_2.19_2018_01_04_12_55.icc`
- Resolve (VIEWER): mit DisplayCal MONITOOR Profil erstellen, dann als LUT laden in Color Settings, File: `U32D970-PreviewViewer-Cali3-rec709- 2018-01-08 20-12 0.3127x 0.329y Rec. 1886 M-S.Rec709.B0.0,2.4eGawn65.cube`
- Resolve (VIDEO): no LUT, Custom Einstellung am Monitor: 
B/c/s: 55/55,80 -- r/g/b: 52/71/66)
(old: b/c/s: 51/70/80 -- r/g/b: 52/69/66)


DAMIT LUTs gehen im VIDEO, muss Hackerl im Resolve disablen:
MENÜ: Davinci Resolve -> Preferences -> User -> UI Settings -> Show all viewers on video output

# ALT
## CUSTOM HDMI kommen von displayport rec709 calibrate3

  - Resolve-Calibration3-Rec709- 2018-01-05 18-28 0.3127x 0.329y S XYZLUT+MTX.Rec709.B0.0,2.4Gawn65.cube
  - Resolve-HDMIVideo-Custom-LargeTest- 2018-01-14 22-21 0.3127x 0.329y S.Rec709.B0.0,2.4Gawn65.cube

  ## newer 
  brigth,con,s: 74/70/60
  r/g/b: 69,60,65

  ## old
  b 71
  c 70
  sharpness 60
  r 71
  g 62
  b 65
  g 2.2 


## CUSTOM HDMI kommend von displayport custom

  - Resolve-HDMIVideo-Custom2Custom-LargeTest- 2018-01-15 00-23 160cdm² 0.3127x 0.329y S.Rec709.B0.0,2.4Gawn65.cube

### Gleich auf HDMI & DisplayPort

  Brightness 61
  C 70
  S 60
  RGB 74/66/58


