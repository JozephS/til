---
kindle-sync:
  bookId: '39633'
  title: 'Fooled by Randomness: The Hidden Role of Chance in Life and in the Markets'
  author: Nassim Taleb
  asin: B002RI9BH6
  lastAnnotatedDate: '2022-03-10'
  bookImageUrl: 'https://m.media-amazon.com/images/I/71AJLkos0yL._SY160.jpg'
  highlightsCount: 73
---
# Fooled by Randomness
## Metadata
* Author: [Nassim Taleb](https://www.amazon.comundefined)
* ASIN: B002RI9BH6
* Reference: https://www.amazon.com/dp/B002RI9BH6
* [Kindle link](kindle://book?action=open&asin=B002RI9BH6)

## Highlights
The reader can see my unusual notion of alternative accounting: $10 million earned through Russian roulette does not have the same value as $10 million earned through the diligent and artful practice of dentistry. They are the same, can buy the same goods, except that one’s dependence on randomness is greater than the other. To an accountant, though, they would be identical; to your next-door neighbor too. Yet, deep down, I cannot help but consider them as qualitatively different. — location: [861](kindle://book?action=open&asin=B002RI9BH6&location=861) ^ref-62382

Share with cyber

---
It is a platitude that children learn only from their own mistakes; they will cease to touch a burning stove only when they are themselves burned; no possible warning by others can lead to developing the smallest form of cautiousness. — location: [1261](kindle://book?action=open&asin=B002RI9BH6&location=1261) ^ref-54027

---
Daniel Kahneman and Amos Tversky — location: [1264](kindle://book?action=open&asin=B002RI9BH6&location=1264) ^ref-16439

---
One day he secreted a pin in his hand before shaking hers. The next day she quickly withdrew her hand as he tried to greet her, but still did not recognize him. Since then plenty of discussions of amnesic patients show some form of learning on the part of people without their being aware of it and without it being stored in conscious memory. — location: [1271](kindle://book?action=open&asin=B002RI9BH6&location=1271) ^ref-1092

---
The blowup, I will repeat, is different from merely incurring a monetary loss; it is losing money when one does not believe that such fact is possible at all. — location: [1283](kindle://book?action=open&asin=B002RI9BH6&location=1283) ^ref-1971

---
They all made claims to the effect that “these times are different” or that “their market was different,” and offered seemingly well-constructed, intellectual arguments (of an economic nature) to justify their claims; — location: [1288](kindle://book?action=open&asin=B002RI9BH6&location=1288) ^ref-35844

---
It is hard to imagine that people who witnessed history did not know at the time how important the moment was. Somehow — location: [1299](kindle://book?action=open&asin=B002RI9BH6&location=1299) ^ref-21450

---
one single observation took place. Our mind will interpret most events not with the preceding ones in mind, but the following ones. — location: [1310](kindle://book?action=open&asin=B002RI9BH6&location=1310) ^ref-52154

---
While we know that history flows forward, it is difficult to realize that we envision it backward. — location: [1312](kindle://book?action=open&asin=B002RI9BH6&location=1312) ^ref-24683

---
Psychologists call this over-estimation of what one knew at the time of the event due to subsequent information the hindsight bias, the “I knew it all along” effect. — location: [1315](kindle://book?action=open&asin=B002RI9BH6&location=1315) ^ref-53812

---
Bad trades catch up with you, it is frequently said in the markets. Mathematicians of probability give that a fancy name: ergodicity. It means, roughly, that (under certain conditions) very long sample paths would end up resembling each other. — location: [1337](kindle://book?action=open&asin=B002RI9BH6&location=1337) ^ref-21273

---
For the difference between noise and information, the topic of this book (noise has more randomness) has an analog: that between journalism and history. — location: [1346](kindle://book?action=open&asin=B002RI9BH6&location=1346) ^ref-1776

---
I will say here that such respect for the time-honored provides arguments to rule out any commerce with the babbling modem journalist and implies a minimal exposure to the media as a guiding principle for someone involved in decision making under uncertainty. If there is anything better than noise in the mass of “urgent” news pounding us, it would be like a needle in a haystack. People do not realize that the media is paid to get your attention. For a journalist, silence — location: [1372](kindle://book?action=open&asin=B002RI9BH6&location=1372) ^ref-45322

---
Shiller then pronounced markets to be not as efficient as established by financial theory (efficient markets meant, in a nutshell, that prices should adapt to all available information in such a way as to be totally unpredictable to us humans and prevent people from deriving profits). — location: [1395](kindle://book?action=open&asin=B002RI9BH6&location=1395) ^ref-17287

---
The defender of the dogmas of modern finance and efficient markets started a fund that took advantage of market inefficiencies! — location: [1405](kindle://book?action=open&asin=B002RI9BH6&location=1405) ^ref-22254

---
thoughtful journalists in the business (I would suggest London’s Anatole Kaletsky and New York’s Jim Grant and Alan Abelson as the underrated representatives of such a class among financial journalists; Gary Stix among scientific journalists); — location: [1410](kindle://book?action=open&asin=B002RI9BH6&location=1410) ^ref-47122

---
an excellent investor, and that he will be expected to earn a return of 15% in excess of Treasury bills, with a 10% error rate per annum (what we call volatility). It means that out of 100 sample paths, we expect close to 68 of them to fall within a band of plus and minus 10% around the 15% excess return, i.e., between 5% and 25% (to be technical; the bell-shaped normal distribution has 68% of all observations falling between –1 and 1 standard deviations). It also means that 95 sample paths would fall between –5% and 35%. — location: [1440](kindle://book?action=open&asin=B002RI9BH6&location=1440) ^ref-49083

---
Probability of success at different scales — location: [1449](kindle://book?action=open&asin=B002RI9BH6&location=1449) ^ref-26918

---
Consider the situation where the dentist examines his portfolio only upon receiving the monthly account from the brokerage house. As 67% of his months will be positive, he incurs only four pangs of pain per annum and eight uplifting experiences. This is the same dentist following the same strategy. — location: [1467](kindle://book?action=open&asin=B002RI9BH6&location=1467) ^ref-18410

---
Finally, this explains why people who look too closely at randomness burn out, their emotions drained by the series of pangs they experience. Regardless — location: [1489](kindle://book?action=open&asin=B002RI9BH6&location=1489) ^ref-5112

---
Clearly, to a scientist, science lies in the rigor of the inference, not in random references to such grandiose concepts as general relativity or quantum indeterminacy. — location: [1533](kindle://book?action=open&asin=B002RI9BH6&location=1533) ^ref-48240

---
Modern life seems to invite us to do the exact opposite; become extremely realistic and intellectual when it comes to such matters as religion and personal behavior, yet as irrational as possible when it comes to matters ruled by randomness (say, portfolio or real estate investments). I have encountered colleagues, — location: [1612](kindle://book?action=open&asin=B002RI9BH6&location=1612) ^ref-10638

---
The Vienna Circle, in their dumping on Hegel-style verbiage-based philosophy, explained that, from a scientific standpoint, it was plain garbage, and, from an artistic point of view, it was inferior to music. — location: [1617](kindle://book?action=open&asin=B002RI9BH6&location=1617) ^ref-27887

---
If I am going to be fooled by randomness, it better be of the beautiful (and harmless) kind. — location: [1620](kindle://book?action=open&asin=B002RI9BH6&location=1620) ^ref-7798

---
When the market started falling, he accumulated more Russian bonds, at an average of around $52. That was Carlos’ trait, average down. The problems, he deemed, had nothing to do with Russia, and it was not some New Jersey fund run by some mad scientist that was going to decide the fate of Russia. “Read my lips: It’s a li-qui-da-tion!” he yelled at those who questioned his buying. By the end of June, his trading revenues for 1998 had dropped from up $60 million to up $20 million. That made him angry. But he calculated that should the market rise back to the pre-New Jersey sell-off, then he would be up $100 million. That was unavoidable, he asserted. These bonds, he said, would never, ever trade below $48. He was risking so little, to possibly make so much. Then came July. The market dropped a bit more. The benchmark Russian bond was now at $43. His positions were underwater, but he increased his stakes. By now he was down $30 million for the year. His bosses were starting to become nervous, but he kept telling them that, after all, Russia would not go under. He repeated the cliché that it was too big to fail. He estimated that bailing them out would cost so little and would benefit the world economy so much that it did not make sense to liquidate his inventory now. “This is the time to buy, not to sell,” he said repeatedly. “These bonds are trading very close to their possible default value.” — location: [1682](kindle://book?action=open&asin=B002RI9BH6&location=1682) ^ref-42970

---
Signs of battle fatigue were starting to show in his behavior. Carlos was getting jumpy and losing some of his composure. He yelled at someone in a meeting: “Stop losses are for schmucks! I am not going to buy high and sell low!” — location: [1698](kindle://book?action=open&asin=B002RI9BH6&location=1698) ^ref-3693

---
“Had we gotten out in October 1997 after our heavy loss we would not have had those excellent 1997 results,” he was also known to repeat. — location: [1700](kindle://book?action=open&asin=B002RI9BH6&location=1700) ^ref-17621

---
at any point in time, the richest traders are often the worst traders. — location: [1724](kindle://book?action=open&asin=B002RI9BH6&location=1724) ^ref-14001

---
At a given time in the market, the most successful traders are likely to be those that are best fit to the latest cycle. This does not happen too often with dentists or pianists—because these professions are more immune to randomness. — location: [1725](kindle://book?action=open&asin=B002RI9BH6&location=1725) ^ref-27686

---
It made the probability 2 in 1,000,000,000,000,000,000,000,000 years. When will John recover from the ordeal? Probably never. The reason is not because John lost money. Losing money is something good traders are accustomed to. It is because he blew up; he lost more than he planned to lose. — location: [1778](kindle://book?action=open&asin=B002RI9BH6&location=1778) ^ref-17649

---
Perhaps we have turned the causality on its head; we consider them good just because they make money. One can make money in the financial markets totally out of randomness. — location: [1832](kindle://book?action=open&asin=B002RI9BH6&location=1832) ^ref-45136

---
What seems to be an evolution may be merely a diversion, and possibly regression. — location: [1850](kindle://book?action=open&asin=B002RI9BH6&location=1850) ^ref-44287

---
For evolution means fitness to one and only one time series, not the average of all the possible environments. — location: [1873](kindle://book?action=open&asin=B002RI9BH6&location=1873) ^ref-18137

---
It can teach us a lot outside of the narrowly defined time series; the broader the look, the better the lesson. In other words, history teaches us to avoid the brand of naive empiricism that consists of learning from casual historical facts. — location: [2038](kindle://book?action=open&asin=B002RI9BH6&location=2038) ^ref-64114

---
Then they “unexpectedly” blow up, lose money for investors, lose their jobs, and switch careers. Then a new period of stability sets in. New currency traders come in with no memory of the bad event. They are drawn to the Mexican peso, and the story repeats itself. — location: [2066](kindle://book?action=open&asin=B002RI9BH6&location=2066) ^ref-32171

---
It is an oddity that most fixed-income financial instruments present rare events. — location: [2068](kindle://book?action=open&asin=B002RI9BH6&location=2068) ^ref-26014

---
Accordingly investors, merely for emotional reasons, will be drawn into strategies that experience rare but large variations. It is called pushing randomness under the rug. — location: [2075](kindle://book?action=open&asin=B002RI9BH6&location=2075) ^ref-44468

---
The agent would prefer the number of losses to be low and the number of gains to be high, rather than optimizing the total performance. — location: [2078](kindle://book?action=open&asin=B002RI9BH6&location=2078) ^ref-25950

---
If the past, by bringing surprises, did not resemble the past previous to it (what I call the past’s past), then why should our future resemble our current past? — location: [2203](kindle://book?action=open&asin=B002RI9BH6&location=2203) ^ref-16249

---
There is something nonphilosophical about investing one’s pride and ego into a “my house/library/car is bigger than that of others in my category”—it is downright foolish to claim to be first in one’s category all the while sitting on a time bomb. To conclude, extreme empiricism, competitiveness, and an absence of logical structure to one’s inference can be a quite explosive combination. — location: [2217](kindle://book?action=open&asin=B002RI9BH6&location=2217) ^ref-50485

---
Many insightful people, such as John Maynard Keynes, independently reached the same conclusions. Sir Karl’s detractors believe that favorably repeating the same experiment again and again should lead to an increased comfort with the notion that “it works.” I came to understand Popper’s position better once I saw the first rare event ravaging a trading room. — location: [2299](kindle://book?action=open&asin=B002RI9BH6&location=2299) ^ref-42283

---
The simple notion of a good model for society that cannot be left open for falsification is totalitarian. — location: [2324](kindle://book?action=open&asin=B002RI9BH6&location=2324) ^ref-31728

The tyranny of the Golden Rule in The Forgotten City Game

---
some sobering information about Popper the man. Witnesses of his private life find him rather un-Popperian. — location: [2327](kindle://book?action=open&asin=B002RI9BH6&location=2327) ^ref-58401

---
we like to emit logical and rational ideas but we do not necessarily enjoy this execution. — location: [2336](kindle://book?action=open&asin=B002RI9BH6&location=2336) ^ref-44110

---
Accordingly, I will use statistics and inductive methods to make aggressive bets, but I will not use them to manage my risks and exposure. — location: [2351](kindle://book?action=open&asin=B002RI9BH6&location=2351) ^ref-36853

Hear hear

---
They trade on ideas based on some observation (that includes past history) but, like the Popperian scientists, they make sure that the costs of being wrong are limited (and their probability is not derived from past data). — location: [2352](kindle://book?action=open&asin=B002RI9BH6&location=2352) ^ref-16990

---
They would then terminate their trade. This is called a stop loss, a predetermined exit point, a protection from the black swan. I find it rarely practiced. — location: [2355](kindle://book?action=open&asin=B002RI9BH6&location=2355) ^ref-52201

Duck

---
The virtue of capitalism is that society can take advantage of people’s greed rather than their benevolence, but there is no need to, in addition, extol such greed as a moral (or intellectual) accomplishment (the — location: [2484](kindle://book?action=open&asin=B002RI9BH6&location=2484) ^ref-12812

---
In my profession one may own a security that benefits from lower market prices, but may not react at all until some critical point. Most people give up before the rewards. — location: [2970](kindle://book?action=open&asin=B002RI9BH6&location=2970) ^ref-49155

---
1,000, which, in your opinion, is exactly fair. Tomorrow night you will have zero or $2,000 in your pocket, each with a 50% probability. In purely mathematical terms, the fair value of a bet is the linear combination of the states, here called the mathematical expectation, i.e., the probabilities of each payoff multiplied by the dollar values at stake (50% multiplied by 0 and 50% multiplied by $2,000 = $1,000). Can you imagine (that is visualize, not compute mathematically) the value being $1,000? We can conjure up one and only one state at a given time, i.e., either 0 or $2,000. Left to — location: [3009](kindle://book?action=open&asin=B002RI9BH6&location=3009) ^ref-17199

---
Who has exerted the most influence on economic thinking over the past two centuries? No, it is not John Maynard Keynes, not Alfred Marshall, not Paul Samuelson, and certainly not Milton Friedman. The answer is two noneconomists: Daniel Kahneman and Amos Tversky, — location: [3073](kindle://book?action=open&asin=B002RI9BH6&location=3073) ^ref-38922

---
entire discipline called behavioral finance and economics has flourished. — location: [3088](kindle://book?action=open&asin=B002RI9BH6&location=3088) ^ref-15534

---
In spite of economists’ envy of physicists, physics is an inherently positive science while economics, particularly microeconomics and financial economics, is predominantly a normative one. — location: [3094](kindle://book?action=open&asin=B002RI9BH6&location=3094) ^ref-60019

---
Normative economics is like religion without the aesthetics. — location: [3095](kindle://book?action=open&asin=B002RI9BH6&location=3095) ^ref-44566

---
A motivated reader can get concentrated in four volumes the collection of the major heuristics and biases papers. — location: [3101](kindle://book?action=open&asin=B002RI9BH6&location=3101) ^ref-2871

---
The absence of a central processing system makes us engage in decisions that can be in conflict with each other. You may prefer apples to oranges, oranges to pears, but pears to apples—it depends on how the choices are presented to you. — location: [3121](kindle://book?action=open&asin=B002RI9BH6&location=3121) ^ref-58612

---
heuristics literature as the “anchoring,” the “affect heuristic,” and the “hindsight bias” — location: [3130](kindle://book?action=open&asin=B002RI9BH6&location=3130) ^ref-44667

---
Say you get a windfall profit of $1 million. The next month you lose $300,000. You adjust to a given wealth (unless of course you are very poor) so the following loss would hurt you emotionally, something that would not have taken place if you received the net amount of $700,000 in one block, or, better, two sums of $350,000 each. — location: [3163](kindle://book?action=open&asin=B002RI9BH6&location=3163) ^ref-60196

---
minutes. This anchoring to a number is the reason people do not react to their total accumulated wealth, but to differences of wealth from whatever number they are currently anchored to. — location: [3177](kindle://book?action=open&asin=B002RI9BH6&location=3177) ^ref-26119

---
By a law of probability called distribution of the maximum of random variables, the maximum of an average is necessarily less volatile than the average maximum. — location: [3440](kindle://book?action=open&asin=B002RI9BH6&location=3440) ^ref-47979

---
difficult to isolate a single cause when there are plenty around. This is called multivariate analysis. For instance, if the stock market can react to U.S. domestic interest rates, the dollar against the yen, the dollar against the European currencies, the European stock markets, the United States balance of payments, United States inflation, and another dozen prime factors, then the journalists need to look at all of these factors, look at their historical effect both in isolation and jointly, look at the stability of such influence, then, after consulting the test statistic, isolate the factor if it is possible to do so. — location: [3480](kindle://book?action=open&asin=B002RI9BH6&location=3480) ^ref-31658

---
Nigel Babbage, has the remarkable attribute of being completely free of any path dependence in his beliefs. — location: [3757](kindle://book?action=open&asin=B002RI9BH6&location=3757) ^ref-63023

---
If you owned no painting, would you still acquire it at the current price? If you would not, then you are said to be married to your position. There is no rational reason to keep a painting you would not buy at its current market rate—only an emotional investment. — location: [3774](kindle://book?action=open&asin=B002RI9BH6&location=3774) ^ref-64717

How to sell

---
we are a bunch of idiots who know nothing and are mistake-prone, but happen to be endowed with the rare privilege of knowing it. — location: [3828](kindle://book?action=open&asin=B002RI9BH6&location=3828) ^ref-47728

---
about scientists in the soft sciences. People confuse science and scientists. Science is great, but individual scientists are dangerous. — location: [3838](kindle://book?action=open&asin=B002RI9BH6&location=3838) ^ref-33026

---
But science is better than scientists. It was said that science evolves from funeral to funeral. — location: [3843](kindle://book?action=open&asin=B002RI9BH6&location=3843) ^ref-30883

---
both competing sects in the ancient world, stoicism and Epicureanism, recommended such control (the difference between the two resides in minor technicalities—neither philosophies meant then what is commonly accepted today in middlebrow culture). — location: [3853](kindle://book?action=open&asin=B002RI9BH6&location=3853) ^ref-24679

---
Recall that epic heroes were judged by their actions, not by the results. — location: [3857](kindle://book?action=open&asin=B002RI9BH6&location=3857) ^ref-7884

---
C. P. Cavafy. Cavafy was an Alexandrian Greek civil servant with a Turkish or Arabic last — location: [3870](kindle://book?action=open&asin=B002RI9BH6&location=3870) ^ref-46904

---
Many people believe it worth learning formal semi-classical Greek — location: [3873](kindle://book?action=open&asin=B002RI9BH6&location=3873) ^ref-31400

---
The God Abandons Antony”) — location: [3877](kindle://book?action=open&asin=B002RI9BH6&location=3877) ^ref-5356

Read or watch

---
While shaken with emotion. No stiff upper lip. There is nothing wrong and undignified with emotions—we are cut to have them. What is wrong is not following the heroic or, at least, the dignified path. That is what stoicism truly means. — location: [3885](kindle://book?action=open&asin=B002RI9BH6&location=3885) ^ref-15781

Truestoicismus

---
Seneca’s Letters from a Stoic, — location: [3896](kindle://book?action=open&asin=B002RI9BH6&location=3896) ^ref-3023

---
