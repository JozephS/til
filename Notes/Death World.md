---
kindle-sync:
  bookId: '47257'
  title: Death World (Undying Mercenaries Book 5)
  author: B. V. Larson
  asin: B00VC05XU4
  lastAnnotatedDate: '2020-08-16'
  bookImageUrl: 'https://m.media-amazon.com/images/I/7162MyfSuyL._SY160.jpg'
  highlightsCount: 1
---
# Death World
## Metadata
* Author: [B. V. Larson](https://www.amazon.com/B-V-Larson/e/B003MESPVM/ref=dp_byline_cont_ebooks_1)
* ASIN: B00VC05XU4
* ISBN: 1511691395
* Reference: https://www.amazon.com/dp/B00VC05XU4
* [Kindle link](kindle://book?action=open&asin=B00VC05XU4)

## Highlights
goat in the woods, — location: [5875](kindle://book?action=open&asin=B00VC05XU4&location=5875) ^ref-31617

---
