
Seit Firmware 3.0 gibt es für die Sony FS7 eine integrierte Noise Suppression Funktion. 

Picture Profile: CINEI Mode mit SLOG3:
Dieser Mode verspricht die größte Dynamic Range bei einer fixen ISO von 2000. Noise suppression hat folgende Einstellungen: low, mid, high. Ich persönlich sehe keinerlei unterschiede, hab aber gelesen dass Details verschwinden. Gebracht hat es doch etwas und ich werds vorerst auf “high” lassen.

Picture Profile: CUSTOM mit SLOG3:
Man verliert zwar die versprochene Dynamic Range von 14f-stops, dafür hat man mehr Freiheiten das Bild in der Kamera zu verändern. Bspw: die ISO -> 2000 und 4000 sehen gut aus und vor allem mit der Noise suppression auch brauchbar. 8000 ist bereits brutal und ungenießbar.

Fazit: Falls es wirklich nötig ist höher als 2000 iso zu schießen, würd ich die Kamera auf die custom settings zurück stufen um in-camera die iso zu boosten. CINEI auf 2000 zu drehen um anschliessend in-post die Luma zu pushen liefert sogar schlechtere Ergebnisse. (Bilder im Anhang). Kann es mir nur so erklären, dass die Kamera intern bereits vorab schärft und deshalb ein besseres Bild mit Noise liefern kann. 


Over und out
