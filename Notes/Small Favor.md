---
kindle-sync:
  bookId: '37574'
  title: 'Small Favor (The Dresden Files, Book 10)'
  author: Jim Butcher
  asin: B000UZNS0O
  lastAnnotatedDate: '2022-12-29'
  bookImageUrl: 'https://m.media-amazon.com/images/I/81mhDOfq50L._SY160.jpg'
  highlightsCount: 1
---
# Small Favor
## Metadata
* Author: [Jim Butcher](https://www.amazon.com/Jim-Butcher/e/B001H6U718/ref=dp_byline_cont_ebooks_1)
* ASIN: B000UZNS0O
* ISBN: 0451461894
* Reference: https://www.amazon.com/dp/B000UZNS0O
* [Kindle link](kindle://book?action=open&asin=B000UZNS0O)

## Highlights
“Three times as many ships and planes have vanished in Lake Michigan as in the Bermuda Triangle.” — location: [4657](kindle://book?action=open&asin=B000UZNS0O&location=4657) ^ref-25277

---
