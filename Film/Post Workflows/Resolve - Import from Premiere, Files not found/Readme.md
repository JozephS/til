IMPORT: Files not found
======================

Issue:
--------

* After importin XML, there are still files not found, even though they are there.
* Timecode not correct errors



Solutions:
------------

1. Drag all files into Resolve BEFORE importing
2. Import xml, yes on still files not found
3. Still files offline, because of timecode troubles:
|3a. If this file is splitted into multiple files, find the first one in file explorer, drag all together into Resolve, tada!

