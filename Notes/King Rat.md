---
kindle-sync:
  bookId: '22004'
  title: 'King Rat: The Fourth Novel of the Asian Saga'
  author: James Clavell
  asin: B00D434A7O
  lastAnnotatedDate: '2021-07-22'
  bookImageUrl: 'https://m.media-amazon.com/images/I/81gB6LYJ+6L._SY160.jpg'
  highlightsCount: 1
---
# King Rat
## Metadata
* Author: [James Clavell](https://www.amazon.com/James-Clavell/e/B000APVE7S/ref=dp_byline_cont_ebooks_1)
* ASIN: B00D434A7O
* ISBN: 0340750685
* Reference: https://www.amazon.com/dp/B00D434A7O
* [Kindle link](kindle://book?action=open&asin=B00D434A7O)

## Highlights
Murmansk — location: [7898](kindle://book?action=open&asin=B00D434A7O&location=7898) ^ref-38513

---
