
![[Screenshot_2022-03-18-09-18-36-123_com.chrome.beta.jpg]]
"Command-Line
Did you ever have the urge to copy something from your buffer in COMMAND-LINE mode? The boring way would be to come back to your buffer in NORMAL mode, copy what you want in a register, come back to COMMAND-LINE mode, and spit what you want. Like in INSERT mode, you can use CTRL+r followed by the name of a register to copy its content in COMMAND-LINE mode.

Or you can use these keystrokes:

CTRL+r CTRL+f - Copy the filename under the buffer’s cursor.
CTRL+r CTRL+w - Copy the word under the buffer’s cursor.
CTRL+r CTRL+a - Copy the WORD under the buffer’s cursor.
CTRL+r CTRL+l - Copy the line under the buffer’s cursor."
 https://thevaluable.dev/vim-veteran/#:~:text=Buffers%20to%20the-,Command%2DLine,r%20CTRL%2Bl%20%2D%20Copy%20the%20line%20under%20the%20buffer%E2%80%99s%20cursor.,-Search