https://www.additudemag.com/flow-state-vs-hyperfocus-adhd/amp/

How to Transition Out of Hyperfocus
One of the problems with getting out of hyperfocus is that whatever you do after will not be as compelling. After all, now your brainpower is so supercharged, energized, or maybe depleted. It’s hard to shift, right? The lure of the dopamine surge is strong.

Transitioning from a hyperfocused, high-dopamine-reward activity to a lower-dopamine one requires a lot of impulse control, emotional regulation, and metacognition. These skills do not come naturally to ADHD brains, especially developing ones, so start by creating and following schedule. Setting time restrictions and using alerts for watching YouTube videos or playing video games can help you (and your kids too) better transition off that hyperfocus activity into whatever comes next.

### tips
[How can you help manage your child’s hyperfocus?](https://www.additudemag.com/new-different-shiny-a-sure-fire-way-to-break-the-grip-of-hyperfocus/)

First, set firm time limits for their high dopamine, hyperfocus activities (usually screen time).

Second, offer them an appealing alternative to this activity and a reward for ending it. For example, “If you get off your video game after the allotted time, we will play cards right away or you can pick the family movie for tonight.”

Third, help your child transition to lower dopamine activities by identifying the ones that really interest them and posting that list in the kitchen. Something that is fun and not a chore. Maybe it’s listening to music, helping with cooking, or riding a stationary bike. It’s hard for a child to transition from a video game and go straight to doing homework or chores. They need an in-between.

# full article

https://www.additudemag.com/flow-state-vs-hyperfocus-adhd/

[[Flow State vs. Hyperfocus_ Understanding Your ADHD Attention.pdf]]
