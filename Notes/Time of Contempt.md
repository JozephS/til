---
kindle-sync:
  bookId: '41455'
  title: 'Time of Contempt: Witcher 2 – Now a major Netflix show (The Witcher)'
  author: Andrzej Sapkowski and David French
  asin: B00BJ5ADLQ
  lastAnnotatedDate: '2016-12-06'
  bookImageUrl: 'https://m.media-amazon.com/images/I/810-cYR-OdL._SY160.jpg'
  highlightsCount: 3
---
# Time of Contempt
## Metadata
* Author: [Andrzej Sapkowski and David French](https://www.amazon.com/Andrzej-Sapkowski/e/B001ICAMAW/ref=dp_byline_cont_ebooks_1)
* ASIN: B00BJ5ADLQ
* ISBN: 0575085088
* Reference: https://www.amazon.com/dp/B00BJ5ADLQ
* [Kindle link](kindle://book?action=open&asin=B00BJ5ADLQ)

## Highlights
authors of the — location: [3171](kindle://book?action=open&asin=B00BJ5ADLQ&location=3171) ^ref-51553

---
deep within the earth. She imbibed — location: [4148](kindle://book?action=open&asin=B00BJ5ADLQ&location=4148) ^ref-59756

---
her. — location: [4545](kindle://book?action=open&asin=B00BJ5ADLQ&location=4545) ^ref-58478

---
