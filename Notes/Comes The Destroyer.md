---
kindle-sync:
  bookId: '52149'
  title: 'Comes The Destroyer: Alien Invasion #5 (Plague Wars Series Book 10)'
  author: David VanDyke
  asin: B00FPOJ27I
  lastAnnotatedDate: '2020-04-10'
  bookImageUrl: 'https://m.media-amazon.com/images/I/91JJaCYS3ML._SY160.jpg'
  highlightsCount: 1
---
# Comes The Destroyer
## Metadata
* Author: [David VanDyke](https://www.amazon.com/David-VanDyke/e/B008EZHPC4/ref=dp_byline_cont_ebooks_1)
* ASIN: B00FPOJ27I
* ISBN: 1626260311
* Reference: https://www.amazon.com/dp/B00FPOJ27I
* [Kindle link](kindle://book?action=open&asin=B00FPOJ27I)

## Highlights
universe, featuring some of your favorite characters as — location: [5295](kindle://book?action=open&asin=B00FPOJ27I&location=5295) ^ref-49155

---
