---
kindle-sync:
  bookId: '39964'
  title: 'Chromed: Rogue: A Cyberpunk Contingency (Future Forfeit Book 2)'
  author: Richard Parry
  asin: B07K732274
  lastAnnotatedDate: '2019-09-13'
  bookImageUrl: 'https://m.media-amazon.com/images/I/71JFz8fLfDL._SY160.jpg'
  highlightsCount: 1
---
# Chromed
## Metadata
* Author: [Richard Parry](https://www.amazon.com/Richard-Parry/e/B00EBQJTI8/ref=dp_byline_cont_ebooks_1)
* ASIN: B07K732274
* ISBN: 0995114870
* Reference: https://www.amazon.com/dp/B07K732274
* [Kindle link](kindle://book?action=open&asin=B07K732274)

## Highlights
strut. Reed decorated the rooftop with their own staff. — location: [3008](kindle://book?action=open&asin=B07K732274&location=3008) ^ref-26503

---
