---
kindle-sync:
  bookId: '24346'
  title: >-
    The New Sell and Sell Short: How To Take Profits, Cut Losses, and Benefit
    From Price Declines (Wiley Trading Book 476)
  author: Alexander Elder
  asin: B004PGMI14
  lastAnnotatedDate: '2021-10-20'
  bookImageUrl: 'https://m.media-amazon.com/images/I/51zbux1aYeL._SY160.jpg'
  highlightsCount: 96
---
# The New Sell and Sell Short
## Metadata
* Author: [Alexander Elder](https://www.amazon.com/Alexander-Elder/e/B000APCUD0/ref=dp_byline_cont_ebooks_1)
* ASIN: B004PGMI14
* ISBN: 978-0470632390
* Reference: https://www.amazon.com/dp/B004PGMI14
* [Kindle link](kindle://book?action=open&asin=B004PGMI14)

## Highlights
The markets inhale and exhale. They get a full chest of air and push it out. They must fall just as certainly as they must rise. To live happily in the markets, you need to get in gear with their rhythm. Any beginner buying stocks knows how to inhale. Knowing when to exhale—when to sell—will set you above the crowd. — location: [396](kindle://book?action=open&asin=B004PGMI14&location=396) ^ref-52106

---
Mechanical Trading Systems: Pairing Trader Psychology with Technical Analysis, — location: [484](kindle://book?action=open&asin=B004PGMI14&location=484) ^ref-26642

Buy

---
will never enter a position against the weekly Impulse system, and you couldn’t pay me to buy above the upper channel line or short below the lower channel line on a daily chart. — location: [517](kindle://book?action=open&asin=B004PGMI14&location=517) ^ref-62988

Guards for impulse

---
“five bullets to a clip”—allowing no more than five indicators on any given chart. You may use six if you desperately need an extra one, but never more than that. For myself, I do well with four: moving averages, envelopes, MACD and the Force Index. — location: [530](kindle://book?action=open&asin=B004PGMI14&location=530) ^ref-58133

---
I am suspicious of classical charting because it is so subjective. I trust only the simplest patterns: support and resistance lines as well as breakouts and fingers, also known as kangaroo tails. I prefer computerized indicators because their signals are clear and not subject to multiple interpretations. Many beginners have a childish faith in the power of technical analysis, often coupled with quite a bit of laziness. Each month I get e-mails from people asking for “the exact settings” of moving averages, MACD, and other indicators. Some say that they want to save time by taking my numbers and skipping research so that they could get right on to trading. Save the time on research! — location: [537](kindle://book?action=open&asin=B004PGMI14&location=537) ^ref-17050

---
charge, as a public service to traders. You can e-mail us at info@elder.com and ask for the template — location: [643](kindle://book?action=open&asin=B004PGMI14&location=643) ^ref-24000

Todo

---
Make mistakes—but do not repeat them! People who like to explore and learn always make mistakes. Whenever I hire people, I tell them that I expect them to make mistakes—it is a part of their job description! Making mistakes is a sign of learning and exploring. Repeating mistakes, on the other hand, is a sign of carelessness, or some psychological problem. The best way to learn from your mistakes is by keeping a trading diary. It allows you to convert the joy of successes and the pain of losses into the bankable gold of experience. — location: [654](kindle://book?action=open&asin=B004PGMI14&location=654) ^ref-31161

---
put two exponential moving averages on every chart and call the space between them the value zone. — location: [822](kindle://book?action=open&asin=B004PGMI14&location=822) ^ref-15986

---
Rises slightly — location: [849](kindle://book?action=open&asin=B004PGMI14&location=849) ^ref-26299

---
All of the above — location: [856](kindle://book?action=open&asin=B004PGMI14&location=856) ^ref-54209

---
3. Relying on TV news vs. advisory services — location: [860](kindle://book?action=open&asin=B004PGMI14&location=860) ^ref-21946

---
4. Price equals value at any given moment. — location: [866](kindle://book?action=open&asin=B004PGMI14&location=866) ^ref-54015

---
A fundamentalist calculates the value of the business that issued a stock. — location: [870](kindle://book?action=open&asin=B004PGMI14&location=870) ^ref-47021

---
A technician looks for repetitive patterns in price data. 4. A pure technician does not care about earnings or corporate news; he only wants to know the stock ticker and price history. — location: [872](kindle://book?action=open&asin=B004PGMI14&location=872) ^ref-23422

---
Greater profit potential per trade — location: [876](kindle://book?action=open&asin=B004PGMI14&location=876) ^ref-43570

---
3. A greater degree of freedom — location: [883](kindle://book?action=open&asin=B004PGMI14&location=883) ^ref-58489

---
“Five bullets to a clip” allows you to use only five indicators. — location: [889](kindle://book?action=open&asin=B004PGMI14&location=889) ^ref-16369

---
If you set your technical system just right, you need not worry about psychology. — location: [893](kindle://book?action=open&asin=B004PGMI14&location=893) ^ref-24460

---
3. If you have a good trading system, discipline is not really an issue. — location: [902](kindle://book?action=open&asin=B004PGMI14&location=902) ^ref-35758

---
Successful traders love the game more than the profits. — location: [908](kindle://book?action=open&asin=B004PGMI14&location=908) ^ref-16046

---
A greater focus on the market — location: [915](kindle://book?action=open&asin=B004PGMI14&location=915) ^ref-21175

---
2. 400 — location: [919](kindle://book?action=open&asin=B004PGMI14&location=919) ^ref-49429

---
4,500 — location: [925](kindle://book?action=open&asin=B004PGMI14&location=925) ^ref-58869

---
It will protect your account from a drawdown caused by a series of bad trades. — location: [930](kindle://book?action=open&asin=B004PGMI14&location=930) ^ref-40850

---
Make money and become a better trader. — location: [937](kindle://book?action=open&asin=B004PGMI14&location=937) ^ref-50120

---
To keep good records — location: [941](kindle://book?action=open&asin=B004PGMI14&location=941) ^ref-7050

---
1. A — location: [948](kindle://book?action=open&asin=B004PGMI14&location=948) ^ref-61664

---
Intelligent people do not make mistakes. — location: [953](kindle://book?action=open&asin=B004PGMI14&location=953) ^ref-26148

---
Repeating mistakes is a sign of impulsivity. — location: [955](kindle://book?action=open&asin=B004PGMI14&location=955) ^ref-33456

---
The diaries of losing trades tend to be more educational than those of winning trades. — location: [959](kindle://book?action=open&asin=B004PGMI14&location=959) ^ref-49604

---
The reason for making this trade 2. The buy grade for long or sell grade for short trades 3. Charts of your trading vehicle at the time of the entry — location: [964](kindle://book?action=open&asin=B004PGMI14&location=964) ^ref-63740

---
The charts are marked up to identify buy or sell signals. — location: [971](kindle://book?action=open&asin=B004PGMI14&location=971) ^ref-27370

---
The stocks you have exited — location: [978](kindle://book?action=open&asin=B004PGMI14&location=978) ^ref-20453

---
It reminds you how long you have been in a trade. — location: [981](kindle://book?action=open&asin=B004PGMI14&location=981) ^ref-49036

---
Maintaining a short list of stocks whose signals you follow. — location: [993](kindle://book?action=open&asin=B004PGMI14&location=993) ^ref-33715

---
2. A and B — location: [997](kindle://book?action=open&asin=B004PGMI14&location=997) ^ref-29112

---
Selling above the midpoint of the bar earns a positive grade. — location: [1003](kindle://book?action=open&asin=B004PGMI14&location=1003) ^ref-18015

---
long-term trend trader can use channels to measure the quality of trades. — location: [1008](kindle://book?action=open&asin=B004PGMI14&location=1008) ^ref-54559

---
A trade capturing over 30% of a channel earns an “A.” — location: [1009](kindle://book?action=open&asin=B004PGMI14&location=1009) ^ref-11866

---
The upper channel line identifies the level of depression and the lower channel line the level of mania in the markets. — location: [1014](kindle://book?action=open&asin=B004PGMI14&location=1014) ^ref-33462

---
Value zone — location: [1020](kindle://book?action=open&asin=B004PGMI14&location=1020) ^ref-8956

C

---
Below value—consider — location: [1021](kindle://book?action=open&asin=B004PGMI14&location=1021) ^ref-55980

A

---
Overvalued—consider — location: [1022](kindle://book?action=open&asin=B004PGMI14&location=1022) ^ref-39571

B

---
A technical comment on a chart pattern that led to a trade — location: [1027](kindle://book?action=open&asin=B004PGMI14&location=1027) ^ref-51451

B

---
A performance rating — location: [1028](kindle://book?action=open&asin=B004PGMI14&location=1028) ^ref-50111

A

---
A psychological comment — location: [1029](kindle://book?action=open&asin=B004PGMI14&location=1029) ^ref-56590

C

---
Value — location: [1043](kindle://book?action=open&asin=B004PGMI14&location=1043) ^ref-27423

B D

---
“People want to make money but do not know what they want from the markets. If I am making a trade, what am I expecting of it? You take a job—you know what your wages and benefits are going to be, what you’re going to be paid for that job. Having a profit target works better for me, although — location: [1301](kindle://book?action=open&asin=B004PGMI14&location=1301) ^ref-8101

---
The power word in life, as well as in trading, is “enough.” You have to decide what will make you happy and set your goals accordingly. The pursuit of your own goals will make you feel in control. To always crave more is to be a slave to greed and advertising. To decide what is enough is to be free. — location: [1504](kindle://book?action=open&asin=B004PGMI14&location=1504) ^ref-46318

Important 

---
When a super-powerful move cannot reach a new high, it tells you that the bulls are starting to run out of breath and reach even higher, but I have long ago given up on trying to catch the absolute top. Remember, the top tick is the most expensive one. — location: [1564](kindle://book?action=open&asin=B004PGMI14&location=1564) ^ref-38665

---
Each price tick reflects an agreement between a buyer and a seller, but it also represents the opinion of the crowd that surrounds these two people. Had the crowd disagreed with either the buyer or the seller, someone would have stepped in, and that trade would have happened at a different price level. — location: [1585](kindle://book?action=open&asin=B004PGMI14&location=1585) ^ref-50415

---
An important footnote about stops—it is perfectly fine to re-enter a market after it hits your stop. Beginners often make a single stab at a stock and leave it alone after it kicks them out. Professionals, on the other hand, see nothing wrong with trying to buy or sell short a stock again and again, like trying to grab a slippery fish, until they finally get a hold of its gills. THE IRON TRIANGLE The main purpose of using stops is to protect yourself from adverse moves by limiting your loss on a trade to a predetermined amount. — location: [1791](kindle://book?action=open&asin=B004PGMI14&location=1791) ^ref-7628

---
You can trail prices with a very short moving average and use its level for a trailing stop. • You can use a Chandelier stop—every time the market makes a new high, move the stop within a certain distance from the top—either a specific price range or a number based on an ATR (average true range). Any time your stock makes a new high, you place your stop within that distance from the top, like hanging a chandelier from the ceiling (this method is described in Come into My Trading Room). • You can use a Parabolic stop (described below). • You can use a SafeZone stop (described below). • You can use a Volatility-Drop stop (described below, for the first time in trading literature). — location: [2076](kindle://book?action=open&asin=B004PGMI14&location=2076) ^ref-33096

---
You can use a Time Stop and get out of your trade if it does not move within a certain time. For example, if you enter a day-trade and the stock does not move within 10 or 15 minutes, it is clearly not doing what you expected, and it is best to scratch that trade. If you put on a swing trade that you expect to last several days, but then a week goes by and the stock is still flat, it is clearly not confirming your analysis and the safest action would be to get out. — location: [2083](kindle://book?action=open&asin=B004PGMI14&location=2083) ^ref-60275

---
The Parabolic system works well in trending markets but leads to whipsaws in trendless markets. It can generate spectacular profits during price trends but chop up an account in a trading range. Do not use it as an automatic trading method. — location: [2109](kindle://book?action=open&asin=B004PGMI14&location=2109) ^ref-4577

---
SAFEZONE STOP SafeZone stops are based on the concept of signal and noise in the financial markets. If the price trend is the signal, then the counter-trend moves are the noise. Engineers build filters to suppress noise and allow the signals to come through. If we can identify and measure market noise, we can place our stops outside of the noise level. This allows us to stay in the trade for as long as the signal identifies a trend. This concept was described in detail in Come into My Trading Room and has since been implemented in several trading programs.6 — location: [2112](kindle://book?action=open&asin=B004PGMI14&location=2112) ^ref-16546

---
I call my trailing stop a Volatility-Drop. If the market is willing to go crazy and have this huge momentum move, I am willing to stay with it. Suppose I use Autoenvelope to set my price target when I enter the trade. The normal width of the envelope is 2.7 standard deviations. If I want to switch to a trailing stop once that target is reached, I will place it one standard deviation tighter—at 1.7 standard deviations. As long as the move continues along the border of a normal envelope, I’ll stay with it, but as soon as the price closes inside of the tighter channel, I’ll be out. A programmer could take it and make it automatic—either intraday, or end-of-day. — location: [2157](kindle://book?action=open&asin=B004PGMI14&location=2157) ^ref-53473

---
alert to the technical signals in the stocks you hold. — location: [2452](kindle://book?action=open&asin=B004PGMI14&location=2452) ^ref-4076

Use nhnl for flatex

---
Red line—new lows. Green line—new highs. — location: [2459](kindle://book?action=open&asin=B004PGMI14&location=2459) ^ref-32770

Watch on color display 

---
The Checklist Manifesto by Dr. Atul Gawande. — location: [2496](kindle://book?action=open&asin=B004PGMI14&location=2496) ^ref-53803

Buy

---
The Black Box, Malcolm MacPherson — location: [2497](kindle://book?action=open&asin=B004PGMI14&location=2497) ^ref-47700

Buy

---
A professional system trader can afford to use a mechanical system precisely because he is capable of discretionary trading. A trading system is an action plan for the market, but no plan can anticipate everything. A degree of judgment is always required, even with the best and most reliable plans. A system automates routine actions and allows you to exercise discretion when needed. And that’s what you need in the markets—a system for finding trades, setting stops, establishing profit targets. — location: [2506](kindle://book?action=open&asin=B004PGMI14&location=2506) ^ref-55975

Systematic Vs discretionary trader

---
Before we focus on selling, just a brief reminder about the decision-making tree for buying. It must begin and end with money management. Your first question must be: Does the 6% Rule allow me to trade? Your last question before putting on a trade must be what size the 2% Rule allows you to trade. We discussed these rules earlier in this book, including the Iron Triangle section. — location: [2513](kindle://book?action=open&asin=B004PGMI14&location=2513) ^ref-25083

Ft

---
a decision tree for selling — location: [2518](kindle://book?action=open&asin=B004PGMI14&location=2518) ^ref-33822

Ft

---
this is a short-term trade, you would need a nearby target in the vicinity of a channel or an envelope. If it is a long-term trade, you can set the profit target farther away, in the vicinity of major support or resistance. Trend traders tend to be long-term oriented and swing traders short-term oriented. — location: [2520](kindle://book?action=open&asin=B004PGMI14&location=2520) ^ref-46980

---
A short-term trader must keep especially close track of his trade grades and be quick to exit soon after his stock hits an A level, after traveling 30% of the channel on the daily chart. — location: [2528](kindle://book?action=open&asin=B004PGMI14&location=2528) ^ref-10172

---
In short-term trading, not only will you have a nearby profit target—you will also have a reasonably tight stop. — location: [2531](kindle://book?action=open&asin=B004PGMI14&location=2531) ^ref-39992

---
Since the iron triangle limits your total risk per trade, the greater the distance to the stop, the fewer shares you may carry. — location: [2537](kindle://book?action=open&asin=B004PGMI14&location=2537) ^ref-51632

---
Listen to different types of “engine noise” for short-term or long-term trades. — location: [2543](kindle://book?action=open&asin=B004PGMI14&location=2543) ^ref-25031

---
A short-term trader may watch the daily charts and indicators for any signs that his trade is becoming overbought and topping out. He would almost certainly run from a bearish divergence of daily MACD-Histogram, but he might also scramble after seeing a fairly minor sign, such as a bearish divergence of the Force index or even a simple downturn of daily MACD-Histogram. — location: [2546](kindle://book?action=open&asin=B004PGMI14&location=2546) ^ref-42572

Ft

---
He should not jump in response to signals on daily charts. If he does, he is almost certain to lose his grip on a long-term trade. He needs to focus on the weekly charts and wait for much louder “engine noise” before getting out of his trade. — location: [2551](kindle://book?action=open&asin=B004PGMI14&location=2551) ^ref-33422

FT
check for.engine noise on longer tf

---
An experienced trader can combine both approaches in a single campaign. He or she may use short-term trading skills by trading around a core position. You can maintain a core long-term position through thick and thin but keep trading shorter-term in the direction of that trade with a portion of your account. — location: [2554](kindle://book?action=open&asin=B004PGMI14&location=2554) ^ref-48845

Far future FT

---
Shorting near the top tends to be harder than buying near the bottom. At the end of a downtrend, markets often appear exhausted and listless, with low volatility and tight price ranges. — location: [3411](kindle://book?action=open&asin=B004PGMI14&location=3411) ^ref-52237

Get long ft right before short

---
One of the key solutions for this problem is money management. You need to short smaller positions and be prepared to reenter — location: [3415](kindle://book?action=open&asin=B004PGMI14&location=3415) ^ref-10407

Ft

---
It pays to trade a size smaller than the maximum permitted by your money management rules. It makes sense to keep some risk capital in reserve. You need to be able to hold on to your bucking horse — location: [3417](kindle://book?action=open&asin=B004PGMI14&location=3417) ^ref-53428

---
Misery loves company and happiness loves solitude. The Short Interest Ratio and Days to Cover help you find stocks whose short side is not overcrowded. — location: [3718](kindle://book?action=open&asin=B004PGMI14&location=3718) ^ref-13771

---
Winning in the Futures Markets by George Angell — location: [3737](kindle://book?action=open&asin=B004PGMI14&location=3737) ^ref-1999

Buy

---
Every options trader should own Lawrence McMillan’s Options as a Strategic Investment. You will probably use it as a handbook rather than read it from cover to cover. Many professionals read Sheldon Natenberg’s Option Volatility and Pricing Strategies. Harvey Friedentag’s Options: Investing Without Fear has a nice angle on covered writing. — location: [3874](kindle://book?action=open&asin=B004PGMI14&location=3874) ^ref-2092

Buy

---
A woman who was a market maker on the floor of the American Exchange once said to me: “Options are a hope business. You can buy hope or sell hope. I am a professional—I sell hope. I come to the floor in the morning, find out what people hope for, price that hope, and sell it to them.” — location: [3888](kindle://book?action=open&asin=B004PGMI14&location=3888) ^ref-17946

---
Profits in the options business are in the writing, not in buying. When you write options, you begin every trade by taking in someone else’s money. A hopeful buyer forks over some money to a writer who is almost always a much more experienced trader. As lawyers say, possession is 90% of the law. The job of options writers is to hang on to buyers’ money and not let it slip away. — location: [3890](kindle://book?action=open&asin=B004PGMI14&location=3890) ^ref-49548

---
Naked Writing The edgy, promising, but dangerous area of the options business is writing naked options. While conservative investors write covered calls against their stocks, naked writers create options out of thin air. They are protected only by their cash and skill and need to be absolutely disciplined in taking profits or cutting losses. When I began working on this chapter, I called Dr. Diane Buffalin in Michigan, an experienced writer of naked options, and asked for a few examples of her trades. Diane sounded bubbly when she talked of her love of writing options. I am doing lots of boring options trades. It’s so simple, I could teach my granddaughter to do it. I said to her—you go to an art high school, you can recognize lines. I can show you—when those lines stop going down, it is time to sell a put. I like taking money in and do not like paying it out. Happiness is selling an option that expires worthless. I had taken my Schedule D to several accountants who said there was a problem with it; buy prices were missing. I had to explain to them that I had sold options and they expired worthless. I love naked selling. The problem with covered selling is that to make money on an option you have to lose some on a stock, and I don’t like that. I look for stocks that I would like to own which are declining. When they stop going down I sell their puts and collect a 10% premium. — location: [3921](kindle://book?action=open&asin=B004PGMI14&location=3921) ^ref-42978

---
A stock can go up, down, or sideways. If you buy a stock or an option, you have one way to make money and two ways to lose. When I sell an option, I have two ways to make money, and sometimes even three. The time to sell an option is when a stock stops moving, and the sharper its move has been, the more they will pay you for the option. — location: [3948](kindle://book?action=open&asin=B004PGMI14&location=3948) ^ref-4407

---
People ask me all the time, “If selling options is so great, how come more people don’t do it?” I think it’s because most options traders like to gamble. They look for tips, but have little patience to play options like chess. Buying options takes little money (about 10% of the stock price), but selling them takes considerable cash. Most brokerage houses won’t let you sell puts unless you have at least $100,000; some require $250,000 and two years’ experience. Brokerage houses say they are trying to protect you, but they let you buy options which have a high probability of losing money; they won’t let you sell options, which have a much lower probability of losing money. — location: [3969](kindle://book?action=open&asin=B004PGMI14&location=3969) ^ref-13934

---
If you sell a put, the worst thing that can happen is that you will buy a stock at a predetermined price. How awful is that? Don’t millions of investors and traders do exactly that every day? So I just sell puts on stocks that I would buy anyway, at a price that I would gladly pay. The only difference is that I get to make the profit without putting my capital into the market. I actually have it in secure bonds, earning interest. And what is the worst thing that can happen if you sell a naked call? You end up with a short position on a volatile stock. Isn’t that exactly what many professional traders do anyway? I love options because I get my profit up-front. And “the worst thing that could happen” is just fine with me. — location: [3974](kindle://book?action=open&asin=B004PGMI14&location=3974) ^ref-47218

---
Shorting is an integral aspect of forex because whenever you buy one currency, you automatically go short another. Buying forex without shorting at the same time is impossible, like finding a coin with only one side. — location: [4032](kindle://book?action=open&asin=B004PGMI14&location=4032) ^ref-63316

Ft

---
New — location: [4516](kindle://book?action=open&asin=B004PGMI14&location=4516) ^ref-36171

Color display 

---
Notice that the MACD Lines did not diverge but rather fell to a new low while MACD-Histogram was diverging; when these two indicators go against one another, it is best to skip entering a trade. — location: [4641](kindle://book?action=open&asin=B004PGMI14&location=4641) ^ref-46785

Ft

---
much more practical approach is to use weekly charts to define a trend and then switch to the daily charts for shorter-term trading in the direction of the weekly chart. Let’s see how a nimble trader can profit from catching shorter waves rather than entire tides. — location: [4651](kindle://book?action=open&asin=B004PGMI14&location=4651) ^ref-31708

---
is important to realize that perfection is the enemy of the good. — location: [4675](kindle://book?action=open&asin=B004PGMI14&location=4675) ^ref-60425

---
MACD divergence on the daily charts is the only signal that will prompt me to disregard the status of the weekly chart. Even if the weekly Impulse is still red, but a daily chart traces a bullish divergence, I will go long. Of course I’ll protect that position with a stop in the vicinity of the recent low. — location: [4748](kindle://book?action=open&asin=B004PGMI14&location=4748) ^ref-65117

Ft

---
The Greatest Trade Ever by Gregory Zuckerman and The Big Short — location: [4753](kindle://book?action=open&asin=B004PGMI14&location=4753) ^ref-22329

Buy

---
Whenever you see this EMA crossover on a weekly chart, get out of your long position. Any trader who followed this essential rule would have sidestepped the great majority of disasters so lustily profiled in mass media over the past decade—not just FRE but also Enron, Global Crossing, and the rest of the pernicious bunch. In — location: [4802](kindle://book?action=open&asin=B004PGMI14&location=4802) ^ref-60764

Ft

---
The Snowball. — location: [4835](kindle://book?action=open&asin=B004PGMI14&location=4835) ^ref-34037

Buy

---
I did not despair—in my experience, crashes are usually followed by a reflex rally and then a slow grind down towards the crash low—a retest of the lows on low volume. — location: [5139](kindle://book?action=open&asin=B004PGMI14&location=5139) ^ref-33414

Ft trader

---
“Every true idealist is after money—because money means freedom, and freedom, in final analysis, means life.” — location: [5163](kindle://book?action=open&asin=B004PGMI14&location=5163) ^ref-28934

---
The Unconventional Trader — location: [5186](kindle://book?action=open&asin=B004PGMI14&location=5186) ^ref-50687

Buy

---
