---
kindle-sync:
  bookId: '26217'
  title: 'Jailhouse Doc: A Doctor in the County Jail'
  author: William Wright M.D.
  asin: B00NI4HVB6
  lastAnnotatedDate: '2015-10-29'
  bookImageUrl: 'https://m.media-amazon.com/images/I/71OF7+d5-zL._SY160.jpg'
  highlightsCount: 1
---
# Jailhouse Doc
## Metadata
* Author: [William Wright M.D.](https://www.amazon.comundefined)
* ASIN: B00NI4HVB6
* Reference: https://www.amazon.com/dp/B00NI4HVB6
* [Kindle link](kindle://book?action=open&asin=B00NI4HVB6)

## Highlights
exact, jails go back a long way. The — location: [540](kindle://book?action=open&asin=B00NI4HVB6&location=540) ^ref-9336

---
