# TIL

Tracks stuff I learned and want to share! Learning is happening!


## How to participate

- On bitbucket.org got to **clone**, to whats described there and clone it on your local disk
- Create a new folder where appropiate, rename it, create a **Readme.md** inside
- Write what you want to share into Readme.md, here is some [Markdown Help](https://bitbucket.org/tutorials/markdowndemo)
- After you are done writing, use this commands in new folder or top directory

> Adds all new files and changes

 `git add -A`

> Describe what you have changed

`git commit -m "{message}"`

> Publish it for everybody on the repository to see

`git push`


If something goes wrong, drop me a line.


