
https://dev.to/jacquibo/why-you-should-keep-a-code-journal-code-journaling-pt-1-of-4-k35
"Awareness is often enough to motivate change.

Simply tracking your food intake will motivate you to alter it. Merely writing down your problems may spark ideas for possible solutions.

The process starts with seeing reality clearly."



"As you work on a project, you will slowly lose motivation without results. You can supplement motivation by changing the project slightly and adding novel little goals that can act like miniature projects to complete in order to have intermediate results. This is what we do naturally, and the process usually spirals out of control. Don’t worry about perfectionism. You need to set a goal that will give a reasonable guarantee that you will outrun your loss of motivation. This means you need to go FAST. Very fast. Neck breakingly fast and shitty. You need to stay entirely focused on exactly what the goal is, and this goal must be clearly defined. In some sense you have to sit down and plan perfectionism out of the way you write the goal. The faster you go, the less chance you give yourself to wiggle out of the goal and make the project more complex than it needs to be. YAGNI is basically the name of the game. Nothing abstracted beyond strict necessity, nothing engineered beyond the scope of the project, you need to engineer the very way you attack the problem. From personal experience, I slip into perfectionism when I lose motivation, and use it as a kind of procrastination. If you retain motivation, you will keep your eyes on the well-defined prize.
Finally, if you can help it, don’t work on projects that you can’t stay motivated for. If you’re doing a startup or research or something where the motivation is entirely internally generated, I think this is the way to do it. If you have a day job well, it’s way harder, and I empathize with you."


"Now, for digital if you use Python, Jupyter is amazing, is almost designed for a tech journal and now also works for R, Julia and Haskell!"
 https://dev.to/jacquibo/why-you-should-keep-a-code-journal-code-journaling-pt-1-of-4-k35#:~:text=Now%2C%20for%20digital%20if%20you%20use%20Python%2C%20Jupyter%20is%20amazing%2C%20is%20almost%20designed%20for%20a%20tech%20journal%20and%20now%20also%20works%20for%20R%2C%20Julia%20and%20Haskell!

 https://news.ycombinator.com/item?id=30589374&utm_source=hackernewsletter&utm_medium=email&utm_term=ask_hn#:~:text=As%20you%20work,empathize%20with%20you.


 ### pause/breaks

 "These pauses give you time to integrate the work that you’ve just completed and let your brain simmer with leftover ideas. Before pausing, leave notes about where you were and what you were thinking about. That’s the important thing. If you’re writing, empty your head. Your note doesn’t have to be grammatically correct. Then when you come back, you know where to begin and what to do."
  https://www.additudemag.com/flow-state-vs-hyperfocus-adhd/amp/#:~:text=These%20pauses%20give,what%20to%20do.
  
