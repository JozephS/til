# Fastest Roundabout Workflow between Premiere and Davinci

## Best Practice

1. Use XML from Premiere, import to Resolve
2. Colorgrade in Resolve
3. Render from Resolve as DNxHD as single Clips
4. Resolve: **Go to Edit**, click on Timeline, export XML from here
5. Premiere: Import Resolve XML
6. Render FX in seperates files and replace original clips in Timeline, do resizing, framing, warp stabilizing also here
7. Prepare Timelines with only seperate FX Clips in it and also name them accordingly so you only have to rerender them in the future
8. Export this XML with the FX clips, import in Resolve
9. Final Stage: Render everything with Resolve
10. Need to changes things: got back to step 2. or step 7.


## Pitfalls / Hints

* Rendering DPX is slower both in Resolve and Premiere
* Rendering 4K Resolution to Full HD as DNxHD in Premiere is quite slow
* Using MXF for rendering in Premiere makes things a little bit faster
* Quote from [CreativeCow](https://forums.creativecow.net/thread/3/951100):
> If you don't mind using the MXF container instead of MOV, it's worth considering for two big reasons: the Avid QuickTime codecs are somewhat broken (prone to highlight errors at high bit depths, incorrect range interpretations/gamma shift across applications), and using 32-bit QuickTime from 64-bit Premiere is slower to encode and decode than using native MXF support.
