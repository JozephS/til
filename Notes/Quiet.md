---
kindle-sync:
  bookId: '63755'
  title: 'Quiet: The Power of Introverts in a World That Can''t Stop Talking'
  author: Susan Cain
  asin: B0074YVW1G
  lastAnnotatedDate: '2015-11-17'
  bookImageUrl: 'https://m.media-amazon.com/images/I/71NDnv8+hvL._SY160.jpg'
  highlightsCount: 2
---
# Quiet
## Metadata
* Author: [Susan Cain](https://www.amazon.com/Susan-Cain/e/B004XVMVYK/ref=dp_byline_cont_ebooks_1)
* ASIN: B0074YVW1G
* ISBN: 0307352145
* Reference: https://www.amazon.com/dp/B0074YVW1G
* [Kindle link](kindle://book?action=open&asin=B0074YVW1G)

## Highlights
a big — location: [1651](kindle://book?action=open&asin=B0074YVW1G&location=1651) ^ref-51380

---
Similarly, Chinese high school students tell researchers that they prefer friends who are “humble” and “altruistic,” “honest” and “hardworking,” while American high school students seek out the “cheerful,” — location: [3118](kindle://book?action=open&asin=B0074YVW1G&location=3118) ^ref-34111

---
