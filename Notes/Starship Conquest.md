---
kindle-sync:
  bookId: '29503'
  title: 'Starship Conquest: (First Conquest) (Stellar Conquest Series Book 1)'
  author: David VanDyke
  asin: B00ROC6600
  lastAnnotatedDate: '2020-04-20'
  bookImageUrl: 'https://m.media-amazon.com/images/I/71HdMyjJ1IL._SY160.jpg'
  highlightsCount: 2
---
# Starship Conquest
## Metadata
* Author: [David VanDyke](https://www.amazon.com/David-VanDyke/e/B008EZHPC4/ref=dp_byline_cont_ebooks_1)
* ASIN: B00ROC6600
* ISBN: 162626239X
* Reference: https://www.amazon.com/dp/B00ROC6600
* [Kindle link](kindle://book?action=open&asin=B00ROC6600)

## Highlights
it would appear the missiles were coming in under the radar intending to strike on a nap-of-the-ground trajectory, in hopes of detonating as close to the base as possible. I’d take that, Absen thought, — location: [1603](kindle://book?action=open&asin=B00ROC6600&location=1603) ^ref-5596

---
Rising from the surface, the six Meme cruisers defending — location: [1605](kindle://book?action=open&asin=B00ROC6600&location=1605) ^ref-38135

---
