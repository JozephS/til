#Avoid different Blacks

##USE CASE

You edit, cc and export stuff with different tools like FCP, PP and Resolve. Sometimes after rendering you see, the blacks are different after exporting or/and when watching via QT, Mac vs PC.

[Learn More](http://www.lightillusion.com/data_tv_levels.htm)

Example (Right: Like it should be, Left: Wrong data level (Problem was: Clip Attributes not set, Data Level in Deliver)
![Example](https://bytebucket.org/JozephS/til/raw/3a7a9adb3f174c0d21039e2f08dda20dba1cc0fa/Film/Post%20Workflows/Data%20vs%20Video%20-%20Different%20Blacks%20in%20PP%2C%20QT%20%26%20Resolve/PPWrongLevels.png)

##Problem Solver

### 1.) Make sure
- It’s not the display (Calibrate it!)

- It’s not different player settings (eg. VLC OpenGL vs DirectX Render)
![VLC](https://bytebucket.org/JozephS/til/raw/d54c676d2e910c60cc39c0fae9c3f633892d6fe5/Film/Post%20Workflows/Data%20vs%20Video%20-%20Different%20Blacks%20in%20PP%2C%20QT%20%26%20Resolve/VLC.png)
- It’s not different data level set in your GPU Settings (nVidia Geforce Settings data vs video level)
![Geforce](https://bytebucket.org/JozephS/til/raw/d54c676d2e910c60cc39c0fae9c3f633892d6fe5/Film/Post%20Workflows/Data%20vs%20Video%20-%20Different%20Blacks%20in%20PP%2C%20QT%20%26%20Resolve/Geforce.png)

### 2.) Get RESOLVE Export Settings Right

#### A.) MEDIA
**SET CLIP ATTRIBUTES**
From Resolve Manual 12, p.150
>
![ResolveManual](https://bytebucket.org/JozephS/til/raw/9ac22ca9b69ddd2c7c8bf3f0f5c560af408b49f1/Film/Post%20Workflows/Data%20vs%20Video%20-%20Different%20Blacks%20in%20PP%2C%20QT%20%26%20Resolve/ResolveManual.png)

     Go to Media/Edit -> Clip Attributes
![ResolveMedia](https://bytebucket.org/JozephS/til/raw/9ac22ca9b69ddd2c7c8bf3f0f5c560af408b49f1/Film/Post%20Workflows/Data%20vs%20Video%20-%20Different%20Blacks%20in%20PP%2C%20QT%20%26%20Resolve/ResolveMedia.png)
     Set to Data Levels
![ResolveClip](https://bytebucket.org/JozephS/til/raw/9ac22ca9b69ddd2c7c8bf3f0f5c560af408b49f1/Film/Post%20Workflows/Data%20vs%20Video%20-%20Different%20Blacks%20in%20PP%2C%20QT%20%26%20Resolve/ResolveClip.png)


#### B.) **DELIVER** Make sure you always have the right settings for data vs video level

     Set “+More Option” -> e.g. Data Level instead of Auto

![Resolve](https://bytebucket.org/JozephS/til/raw/d54c676d2e910c60cc39c0fae9c3f633892d6fe5/Film/Post%20Workflows/Data%20vs%20Video%20-%20Different%20Blacks%20in%20PP%2C%20QT%20%26%20Resolve/Resolve.png)
### 3.) WORKFLOW FOR 5D mk II h.264 Codecs

>   - Set **Clip Attributes** to **Data** (should now look the same as PP)
>   - Set **DELIVER** to **Video** Levels (should look the “same” in PP after export)
