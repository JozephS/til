---
kindle-sync:
  bookId: '11296'
  title: 'Shorter: Work Better, Smarter, and Less—Here''s How'
  author: Alex Soojung-Kim Pang
  asin: B07V32PZMT
  lastAnnotatedDate: '2021-10-01'
  bookImageUrl: 'https://m.media-amazon.com/images/I/71jLakQZLAL._SY160.jpg'
  highlightsCount: 1
---
# Shorter
## Metadata
* Author: [Alex Soojung-Kim Pang](https://www.amazon.com/Alex-Soojung-Kim-Pang/e/B001KHJ91I/ref=dp_byline_cont_ebooks_1)
* ASIN: B07V32PZMT
* ISBN: 1541730712
* Reference: https://www.amazon.com/dp/B07V32PZMT
* [Kindle link](kindle://book?action=open&asin=B07V32PZMT)

## Highlights
For engineers accustomed to mastering difficult technologies, complexity represented power, not a problem, — location: [301](kindle://book?action=open&asin=B07V32PZMT&location=301) ^ref-40581

---
