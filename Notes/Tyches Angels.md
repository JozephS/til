---
kindle-sync:
  bookId: '53560'
  title: >-
    Tyche's Angels: A Space Opera Adventure Science Fiction Epic (Ezeroc Wars
    Book 6)
  author: Richard Parry
  asin: B07DHBS7FP
  lastAnnotatedDate: '2019-12-23'
  bookImageUrl: 'https://m.media-amazon.com/images/I/91KcZhKkD5L._SY160.jpg'
  highlightsCount: 4
---
# Tyches Angels
## Metadata
* Author: [Richard Parry](https://www.amazon.com/Richard-Parry/e/B00EBQJTI8/ref=dp_byline_cont_ebooks_1)
* ASIN: B07DHBS7FP
* ISBN: 0473445506
* Reference: https://www.amazon.com/dp/B07DHBS7FP
* [Kindle link](kindle://book?action=open&asin=B07DHBS7FP)

## Highlights
alien ship didn’t slow. Made sense. The Empire had the schematics on the railgun array. It fired kilogram slugs of — location: [4215](kindle://book?action=open&asin=B07DHBS7FP&location=4215) ^ref-53278

---
two at a time. Not three. All, at once. Grace was at his right side, blade — location: [4456](kindle://book?action=open&asin=B07DHBS7FP&location=4456) ^ref-7511

---
just for a moment, he could take a still of Kazuo’s expression. The man was stock-still, wide-mouthed. Then he snarled, and Saveria Complex — location: [4459](kindle://book?action=open&asin=B07DHBS7FP&location=4459) ^ref-44073

---
compulsion, even though he still held — location: [4462](kindle://book?action=open&asin=B07DHBS7FP&location=4462) ^ref-11249

---
