---
kindle-sync:
  bookId: '36221'
  title: 'Two Roads Diverged: Trading Divergences (Trading with Dr Elder Book 2)'
  author: Dr Alexander Elder
  asin: B006V3CWDS
  lastAnnotatedDate: '2021-12-01'
  bookImageUrl: 'https://m.media-amazon.com/images/I/71-4O83GYYL._SY160.jpg'
  highlightsCount: 13
---
# Two Roads Diverged
## Metadata
* Author: [Dr Alexander Elder](https://www.amazon.com/Dr-Alexander-Elder/e/B0075R0BBO/ref=dp_byline_cont_ebooks_1)
* ASIN: B006V3CWDS
* Reference: https://www.amazon.com/dp/B006V3CWDS
* [Kindle link](kindle://book?action=open&asin=B006V3CWDS)

## Highlights
Technical Analysis of Financial Markets — location: [94](kindle://book?action=open&asin=B006V3CWDS&location=94) ^ref-53775

Buy

---
Remember this rule: when trying to find a divergence, first look at the pattern of an indicator and later at the pattern of prices. — location: [136](kindle://book?action=open&asin=B006V3CWDS&location=136) ^ref-58046

Use for ft

---
One of the key differences between amateurs and pros is that when a beginner gets stopped out, he feels disgusted and moves on to other stocks.  Professionals, on the other hand, often attempt multiple entries, using fairly tight stops. — location: [179](kindle://book?action=open&asin=B006V3CWDS&location=179) ^ref-59716

---
Bullish divergences work best when the distance between two bottoms is between 20 and 50 bars – the closer to 20 the better. — location: [336](kindle://book?action=open&asin=B006V3CWDS&location=336) ^ref-458

Ft

---
Another good sign for a bullish divergence is when the second bottom is less than half the depth of the first one. — location: [337](kindle://book?action=open&asin=B006V3CWDS&location=337) ^ref-40840

Ft

---
Place your profit target in the vicinity of the upper channel line in the same timeframe as the divergence.  Keep in mind that divergences tend to give very strong signals, and such targets are often exceeded.  Still, the quickest and most reliable gains tend to occur during the initial explosion from the second bottom of MACD-Histogram. — location: [339](kindle://book?action=open&asin=B006V3CWDS&location=339) ^ref-57607

Ft

---
use Triple Screen, and make strategic decisions in a longer timeframe, in this case a 25-minute chart.  If the 25-minute chart allows me to go long, and all I need before buying is an uptick of MACD on a 5-minute chart, I will enter as soon as a signal flashes during a bar.  If the longer timeframe looks good, I’ll enter intra-bar on a shorter timeframe, otherwise I’ll wait for the next bar. — location: [414](kindle://book?action=open&asin=B006V3CWDS&location=414) ^ref-38677

Ft

---
Divergences in the 13-bar EMA of Force Index are especially valuable. — location: [485](kindle://book?action=open&asin=B006V3CWDS&location=485) ^ref-48849

Ft

---
New Low Index (NHNL).  I consider it the best leading indicator of the stock market. — location: [500](kindle://book?action=open&asin=B006V3CWDS&location=500) ^ref-3117

Ft

---
Profits could be taken near the upper channel line.  They absolutely should be taken when prices stall above their upper channel line.   This chart has several other important features.  Notice a bullish pattern of MACD Lines in area C and of course the false downside breakout at bottom C.  A false breakout occurs when prices break below support but cannot follow through and rally above the line of support, giving a buy signal.   We could have filled hundreds pages with examples of bullish and bearish divergences on intraday charts – hourly, half-hourly, 10-minute, 5-minute, you name it.  Perhaps you can do that in your own trading diary.  Just keep in mind that the concept of divergences applies to all timeframes. Return to the Top — location: [568](kindle://book?action=open&asin=B006V3CWDS&location=568) ^ref-5636

---
For example:                     omit stocks that trade fewer than a million or at least half a million shares a day                   do not look for bullish divergences in stocks cheaper than $3                   do not look for bearish divergences in stocks cheaper than $10   It pays to eliminate low-volume stocks from a scan because low volume reflects low public participation. — location: [587](kindle://book?action=open&asin=B006V3CWDS&location=587) ^ref-22848

---
A scan for bullish divergences performs the following steps, in this sequence: — location: [604](kindle://book?action=open&asin=B006V3CWDS&location=604) ^ref-47945

Ft

---
MACD-Histogram drops to the lowest low of 100 bars (this number can be changed).  This identifies bottom A of the potential A-B-C bullish divergence. — location: [607](kindle://book?action=open&asin=B006V3CWDS&location=607) ^ref-43338

---
