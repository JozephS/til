Imported DPX with SceneDetection is too long
----------------------------------------------

If you did your Scene Detection sloppy you probably have some scenes that are only 1 frame long. But Davinci detects it then as media type: image and imports with a different lengths (default: 5s). Of course this messes with rouy overall length.

Go to **Project Settings -> Edit** and change Image length to 1 Frame
