---
kindle-sync:
  bookId: '5186'
  title: >-
    Tyche's Demons: A Space Opera Adventure Science Fiction Epic (Ezeroc Wars
    Book 4)
  author: Richard Parry
  asin: B07BRG8RYV
  lastAnnotatedDate: '2019-11-17'
  bookImageUrl: 'https://m.media-amazon.com/images/I/91-yf9FnIuL._SY160.jpg'
  highlightsCount: 1
---
# Tyches Demons
## Metadata
* Author: [Richard Parry](https://www.amazon.com/Richard-Parry/e/B00EBQJTI8/ref=dp_byline_cont_ebooks_1)
* ASIN: B07BRG8RYV
* ISBN: 0995109079
* Reference: https://www.amazon.com/dp/B07BRG8RYV
* [Kindle link](kindle://book?action=open&asin=B07BRG8RYV)

## Highlights
her crew need to test their skill and their luck to survive. Will Grace and Nate be able to work together to get away? Or will fears and rivalries from the past destroy humanity’s hopes, ceding victory to the Ezeroc? Tyche’s Journey Book 1: https://www.books2read.com/TychesFlight Book 2: https://www.books2read.com/TychesDeceit Book 3: https://www.books2read.com/TychesCrown Tyche’s Progeny Book 4: https://www.books2read.com/TychesDemons Book 5: https://www.books2read.com/TychesGhosts Book 6: https://www.books2read.com/TychesAngels Tyche’s Fallen Book 7: https://www.books2read.com/TycheForever Book 8: https://www.books2read.com/TychesLost Book 9: https://www.books2read.com/TychesCrusade Tyche Origins An empire falls. Before there — location: [4418](kindle://book?action=open&asin=B07BRG8RYV&location=4418) ^ref-62914

---
